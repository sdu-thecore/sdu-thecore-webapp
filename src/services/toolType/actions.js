import { snakeCase } from 'lodash';
import {
  TOOL_TYPE_READ_MANY,
  TOOL_TYPE_CREATE_ONE,
  TOOL_TYPE_UPDATE_ONE,
  TOOL_TYPE_DELETE_ONE,
  REQUEST
} from '../../constants';
import { ENDPOINT_TOOL_TYPES, ENDPOINT_TOOL_TYPE } from '../../endpoints';
import { walkKeys, walkValues, trim } from '../../util';

export const readToolTypes = (sortBy = 'name') => (dispatch, getState) => {
  const { pageSize, nextPage } = getState().node;
  const offset = pageSize * nextPage;
  dispatch({
    type: TOOL_TYPE_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_TOOL_TYPES({
          limit: pageSize,
          sortOrder: 'asc',
          sortBy,
          offset
        })
      }
    }
  });
};

export const createToolType = entity => ({
  type: TOOL_TYPE_CREATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'POST',
      url: ENDPOINT_TOOL_TYPES(),
      data: walkKeys(snakeCase)(walkValues(trim)(entity))
    }
  },
  entity
});

export const updateToolType = (id, entity) => ({
  type: TOOL_TYPE_UPDATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'PATCH',
      url: ENDPOINT_TOOL_TYPE(id),
      data: walkKeys(snakeCase)(walkValues(trim)(entity))
    }
  },
  id,
  entity
});

export const deleteToolType = id => ({
  type: TOOL_TYPE_DELETE_ONE + REQUEST,
  payload: {
    request: {
      method: 'DELETE',
      url: ENDPOINT_TOOL_TYPE(id)
    }
  },
  id
});
