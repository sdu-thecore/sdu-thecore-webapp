import { camelCase } from 'lodash';
import defaultState from './state';
import {
  USER_READ_OWN,
  USER_READ_MANY,
  USER_UPDATE_ONE,
  USER_READ_ONE,
  AUTH_LOGOUT,
  REQUEST,
  SUCCESS,
} from '../../constants';
import { walkKeys, arrayToObject } from '../../util';

const user = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case USER_READ_MANY + REQUEST: {
      return { ...state, loading: true };
    }
    case USER_READ_MANY + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data, count } = action.payload.data;
        const newUsers = arrayToObject(walkKeys(camelCase)(data));
        const users = { ...state.users, ...newUsers };
        const currentCount = Object.keys(users).length;
        const loadedCount = Object.keys(newUsers).length;
        const nextPage =
          loadedCount < state.pageSize ? state.nextPage : state.nextPage + 1;
        return {
          ...state,
          loading: false,
          hasNextPage: count > currentCount,
          nextPage: state.hasNextPage ? nextPage : 0,
          count,
          users,
        };
      }
      return state;
    }
    case USER_READ_OWN + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          profile: walkKeys(camelCase)(data),
        };
      }
      return state;
    }
    case USER_UPDATE_ONE + REQUEST: {
      return {
        ...state,
        users: {
          ...state.users,
          [action.userId]: {
            ...state.users[action.userId],
            ...action.update,
          },
        },
      };
    }
    case USER_UPDATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          users: {
            ...state.users,
            [data.id]: {
              ...state.users[data.id],
              ...walkKeys(camelCase)(data),
            },
          },
        };
      }
      return state;
    }
    case USER_READ_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          users: {
            ...state.users,
            [data.id]: {
              ...state.users[data.id],
              ...walkKeys(camelCase)(data),
            },
          },
        };
      }
      return state;
    }
    case AUTH_LOGOUT: {
      return {
        ...state,
        profile: defaultState.profile,
      };
    }
    default: {
      return state;
    }
  }
};

export default user;
