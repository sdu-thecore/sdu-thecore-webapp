import { snakeCase } from 'lodash';
import {
  REQUEST,
  USER_READ_OWN,
  USER_READ_MANY,
  USER_UPDATE_ONE,
  USER_READ_ONE,
} from '../../constants';
import { ENDPOINT_ME, ENDPOINT_USERS, ENDPOINT_USER } from '../../endpoints';
import { walkKeys, walkValues, trim } from '../../util';

export const readMe = () => ({
  type: USER_READ_OWN + REQUEST,
  payload: {
    request: {
      url: ENDPOINT_ME(),
    },
  },
});

export const readUsers = (sortBy = 'display_name') => (dispatch, getState) => {
  const { pageSize, nextPage } = getState().user;
  const offset = pageSize * nextPage;
  dispatch({
    type: USER_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_USERS({
          limit: pageSize,
          sortBy,
          offset,
        }),
      },
    },
  });
};

export const updateUser = (userId, update) => (dispatch) => {
  dispatch({
    type: USER_UPDATE_ONE + REQUEST,
    payload: {
      request: {
        method: 'PATCH',
        url: ENDPOINT_USER(userId),
        data: walkKeys(snakeCase)(walkValues(trim)(update)),
      },
    },
    userId,
    update,
  });
};

export const readUser = (userId) => ({
  type: USER_READ_ONE + REQUEST,
  payload: {
    request: {
      url: ENDPOINT_USER(userId),
    },
  },
  userId,
});
