export default {
  issue: null,
  form: {
    fields: {
      type: '',
      title: '',
      description: ''
    },
    errors: [],
    loading: false
  }
};
