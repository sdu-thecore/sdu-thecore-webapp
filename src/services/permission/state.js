export default {
  permissions: {},
  pageSize: 20,
  nextPage: 0,
  hasNextPage: true,
  count: 0,
  loading: false,
  isUpdatingOne: false,
  isCreatingOne: false,
  isDeletingOne: false
};
