import { snakeCase } from 'lodash';
import { ENDPOINT_PERMISSIONS, ENDPOINT_PERMISSION } from '../../endpoints';
import { walkKeys, walkValues, trim } from '../../util';
import {
  PERMISSION_READ_MANY,
  PERMISSION_READ_ONE,
  PERMISSION_CREATE_ONE,
  PERMISSION_UPDATE_ONE,
  PERMISSION_DELETE_ONE,
  REQUEST,
} from '../../constants';

export const readPermissions = (sortBy = 'abbreviation') => (
  dispatch,
  getState
) => {
  const { pageSize, nextPage } = getState().permission;
  const offset = pageSize * nextPage;
  dispatch({
    type: PERMISSION_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_PERMISSIONS({
          limit: pageSize,
          sortBy,
          offset,
        }),
      },
    },
  });
};

export const createPermission = (entity) => ({
  type: PERMISSION_CREATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'POST',
      url: ENDPOINT_PERMISSIONS(),
      data: walkKeys(snakeCase)(walkValues(trim)(entity)),
    },
  },
  entity,
});

export const updatePermission = (id, entity) => ({
  type: PERMISSION_UPDATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'PATCH',
      url: ENDPOINT_PERMISSION(id),
      data: walkKeys(snakeCase)(walkValues(trim)(entity)),
    },
  },
  id,
  entity,
});

export const deletePermission = (id) => ({
  type: PERMISSION_DELETE_ONE + REQUEST,
  payload: {
    request: {
      method: 'DELETE',
      url: ENDPOINT_PERMISSION(id),
    },
  },
  id,
});

export const readPermission = (id) => ({
  type: PERMISSION_READ_ONE + REQUEST,
  payload: {
    request: {
      url: ENDPOINT_PERMISSION(id),
    },
  },
  id,
});
