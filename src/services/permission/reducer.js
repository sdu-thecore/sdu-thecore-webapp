import { camelCase } from 'lodash';
import defaultState from './state';
import { walkKeys, arrayToObject } from '../../util';
import {
  PERMISSION_READ_MANY,
  PERMISSION_CREATE_ONE,
  PERMISSION_DELETE_ONE,
  PERMISSION_READ_ONE,
  REQUEST,
  SUCCESS,
  FAIL,
  PERMISSION_UPDATE_ONE,
} from '../../constants';

const permission = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case PERMISSION_READ_MANY + REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case PERMISSION_READ_MANY + REQUEST + FAIL: {
      return {
        ...state,
        loading: false,
      };
    }
    case PERMISSION_READ_MANY + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data, count } = action.payload.data;
        const newPermissions = arrayToObject(walkKeys(camelCase)(data));
        const permissions = {
          ...state.permissions,
          ...newPermissions,
        };
        const currentCount = Object.keys(permissions).length;
        const loadedCount = Object.keys(newPermissions).length;
        const nextPage =
          loadedCount < state.pageSize ? state.nextPage : state.nextPage + 1;
        return {
          ...state,
          loading: false,
          hasNextPage: count > currentCount,
          nextPage: state.hasNextPage ? nextPage : 0,
          count,
          permissions,
        };
      }
      return {
        ...state,
        loading: false,
      };
    }
    case PERMISSION_CREATE_ONE + REQUEST: {
      return {
        ...state,
        isCreatingOne: true,
      };
    }
    case PERMISSION_CREATE_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isCreatingOne: false,
      };
    }
    case PERMISSION_CREATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        const newPermissions = arrayToObject(walkKeys(camelCase)([data]));
        const permissions = {
          ...state.permissions,
          ...newPermissions,
        };
        return {
          ...state,
          isCreatingOne: false,
          count: state.count + 1,
          hasNextPage: true,
          permissions,
        };
      }
      return {
        ...state,
        isCreatingOne: false,
      };
    }
    case PERMISSION_DELETE_ONE + REQUEST: {
      return {
        ...state,
        isDeletingOne: true,
      };
    }
    case PERMISSION_DELETE_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isDeletingOne: false,
      };
    }
    case PERMISSION_DELETE_ONE + REQUEST + SUCCESS: {
      if (
        action.meta &&
        action.meta.previousAction &&
        action.meta.previousAction.id
      ) {
        const { previousAction } = action.meta;
        const permissionKeys = Object.keys(state.permissions);
        const filterKeys = permissionKeys.filter(
          (id) => id !== previousAction.id
        );
        const permissions = filterKeys.reduce(
          (obj, key) => ({
            ...obj,
            [key]: state.permissions[key],
          }),
          {}
        );
        return {
          ...state,
          isDeletingOne: false,
          count: state.count - 1,
          permissions,
        };
      }
      return {
        ...state,
        isDeletingOne: false,
      };
    }
    case PERMISSION_UPDATE_ONE + REQUEST: {
      return {
        ...state,
        isUpdatingOne: true,
      };
    }
    case PERMISSION_UPDATE_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isUpdatingOne: false,
      };
    }
    case PERMISSION_UPDATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        const newPermissions = arrayToObject(walkKeys(camelCase)([data]));
        const permissions = {
          ...state.permissions,
          ...newPermissions,
        };
        return {
          ...state,
          isUpdatingOne: false,
          hasNextPage: true,
          permissions,
        };
      }
      return {
        ...state,
        isUpdatingOne: false,
      };
    }
    case PERMISSION_READ_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          permissions: {
            ...state.permissions,
            [data.id]: data,
          },
        };
      }
      return state;
    }
    default: {
      return state;
    }
  }
};

export default permission;
