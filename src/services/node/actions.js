import { snakeCase } from 'lodash';
import { walkKeys, walkValues, trim } from '../../util';
import {
  NODE_READ_MANY,
  NODE_SESSION_UPDATE_ONE,
  REQUEST,
  NODE_DELETE_ONE,
  NODE_UPDATE_ONE,
} from '../../constants';
import {
  ENDPOINT_NODES,
  ENDPOINT_NODE_SESSION,
  ENDPOINT_NODE,
} from '../../endpoints';

export const readNodes = (sortBy = 'display_name') => (dispatch, getState) => {
  const { pageSize, nextPage } = getState().node;
  const offset = pageSize * nextPage;
  dispatch({
    type: NODE_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_NODES({
          limit: pageSize,
          sortBy,
          offset,
          populate: 'sessions',
        }),
      },
    },
  });
};

export const updateNodeSession = (nodeId, sessionId, update) => (dispatch) => {
  dispatch({
    type: NODE_SESSION_UPDATE_ONE + REQUEST,
    payload: {
      request: {
        method: 'PATCH',
        url: ENDPOINT_NODE_SESSION(nodeId, sessionId),
        data: walkKeys(snakeCase)(walkValues(trim)(update)),
      },
    },
    nodeId,
    sessionId,
    update,
  });
};

export const updateNode = (nodeId, update) => (dispatch) => {
  dispatch({
    type: NODE_UPDATE_ONE + REQUEST,
    payload: {
      request: {
        method: 'PATCH',
        url: ENDPOINT_NODE(nodeId),
        data: walkKeys(snakeCase)(walkValues(trim)(update)),
      },
    },
    nodeId,
  });
};

export const deleteNode = (nodeId) => (dispatch) => {
  dispatch({
    type: NODE_DELETE_ONE + REQUEST,
    payload: {
      request: {
        method: 'DELETE',
        url: ENDPOINT_NODE(nodeId),
      },
    },
    nodeId,
  });
};
