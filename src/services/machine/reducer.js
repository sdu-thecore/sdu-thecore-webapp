import { camelCase } from 'lodash';
import defaultState from './state';
import {
  MACHINE_READ_MANY,
  MACHINE_READ_NEW,
  MACHINE_READ_ONE,
  REQUEST,
  SUCCESS,
  MACHINE_SET_NEW,
  MACHINE_CREATE_ONE,
  MACHINE_FORM_OPEN,
  MACHINE_DELETE_ONE,
  MACHINE_UPDATE_ONE,
} from '../../constants';
import { walkKeys, arrayToObject } from '../../util';

const machine = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case MACHINE_READ_MANY + REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case MACHINE_READ_MANY + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data, count } = action.payload.data;
        const newMachines = arrayToObject(walkKeys(camelCase)(data));
        const machines = { ...state.machines, ...newMachines };
        const currentCount = Object.keys(machines).length;
        const loadedCount = Object.keys(newMachines).length;
        const nextPage =
          loadedCount < state.pageSize ? state.nextPage : state.nextPage + 1;
        return {
          ...state,
          loading: false,
          hasNextPage: count > currentCount,
          nextPage: state.hasNextPage ? nextPage : 0,
          count,
          machines,
        };
      }
      return state;
    }
    case MACHINE_READ_NEW + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const newMachine = action.payload.data.data;
        return {
          ...state,
          new: {
            attributes: newMachine.attributes || state.new.attributes || [],
            id: newMachine.id || state.new.id || '',
            job: newMachine.job || state.new.job || '',
            node: newMachine.node || state.new.node || '',
            number: newMachine.number || state.new.number || 0,
            permission: newMachine.permission || state.new.permission || '',
            status: newMachine.status || state.new.status || 'maintenance',
          },
        };
      }
      return state;
    }
    case MACHINE_SET_NEW: {
      const { newMachine } = action;
      return {
        ...state,
        new: {
          attributes: newMachine.attributes || state.new.attributes || [],
          id: newMachine.id || state.new.id || '',
          job: newMachine.job || state.new.job || '',
          node: newMachine.node || state.new.node || '',
          number: newMachine.number || state.new.number || 0,
          permission: newMachine.permission || state.new.permission || '',
          status: newMachine.status || state.new.status || 'maintenance',
        },
      };
    }
    case MACHINE_READ_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          machines: {
            ...state.machines,
            [data.id]: data,
          },
        };
      }
      return state;
    }
    case MACHINE_UPDATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          machines: {
            ...state.machines,
            [data.id]: data,
          },
        };
      }
      return state;
    }
    case MACHINE_CREATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        return {
          ...state,
          new: defaultState.new,
          newFormOpen: false,
          machines: {
            ...state.machines,
            [data.id]: data,
          },
        };
      }
      return state;
    }
    case MACHINE_DELETE_ONE + REQUEST + SUCCESS: {
      const { machineId } = action.meta.previousAction;
      return {
        ...state,
        count: state.count - 1,
        machines: Object.keys(state.machines).reduce(
          (remainingMachines, id) => {
            const machines = { ...remainingMachines };
            if (id !== machineId) {
              machines[id] = state.machines[id];
            }
            return machines;
          },
          {}
        ),
      };
    }
    case MACHINE_FORM_OPEN: {
      return {
        ...state,
        newFormOpen: action.open,
      };
    }
    default: {
      return state;
    }
  }
};

export default machine;
