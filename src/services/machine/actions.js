import { snakeCase } from 'lodash';
import {
  MACHINE_READ_MANY,
  REQUEST,
  MACHINE_READ_NEW,
  MACHINE_SET_NEW,
  MACHINE_CREATE_ONE,
  MACHINE_READ_ONE,
  MACHINE_FORM_OPEN,
  MACHINE_DELETE_ONE,
  MACHINE_UPDATE_ONE,
} from '../../constants';
import {
  ENDPOINT_MACHINES,
  ENDPOINT_MACHINE_NEW,
  ENDPOINT_MACHINE,
} from '../../endpoints';
import { walkKeys, trim, walkValues } from '../../util';

export const setMachineNew = (newMachine) => (dispatch) => {
  dispatch({
    type: MACHINE_SET_NEW,
    newMachine,
  });
};

export const readMachines = (sortBy = 'created') => (dispatch, getState) => {
  const { pageSize, nextPage } = getState().node;
  const offset = pageSize * nextPage;
  dispatch({
    type: MACHINE_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_MACHINES({
          limit: pageSize,
          sortBy,
          offset,
        }),
      },
    },
  });
};

export const readMachineNew = (permissionId) => (dispatch) => {
  dispatch({
    type: MACHINE_READ_NEW + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_MACHINE_NEW({ permission: permissionId }),
      },
    },
  });
};

export const createMachine = () => (dispatch, getState) => {
  const newData = getState().machine.new;
  const newMachine = {
    node: newData.node,
    status: newData.status,
    permission: newData.permission,
  };
  dispatch({
    type: MACHINE_CREATE_ONE + REQUEST,
    payload: {
      request: {
        method: 'POST',
        url: ENDPOINT_MACHINES(),
        data: walkKeys(snakeCase)(walkValues(trim)(newMachine)),
      },
    },
  });
};

export const readMachine = (machineId) => (dispatch) => {
  dispatch({
    type: MACHINE_READ_ONE + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_MACHINE(machineId),
      },
    },
  });
};

export const updateMachine = (machineId, update) => ({
  type: MACHINE_UPDATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'PATCH',
      url: ENDPOINT_MACHINE(machineId),
      data: walkKeys(snakeCase)(walkValues(trim)(update)),
    },
  },
  machineId,
  update,
});

export const deleteMachine = (machineId) => (dispatch) => {
  dispatch({
    type: MACHINE_DELETE_ONE + REQUEST,
    payload: {
      request: {
        method: 'DELETE',
        url: ENDPOINT_MACHINE(machineId),
      },
    },
    machineId,
  });
};

export const openMachineForm = (open) => ({
  type: MACHINE_FORM_OPEN,
  open,
});
