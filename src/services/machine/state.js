export default {
  new: {
    node: '',
    number: 0,
    permission: '',
    status: 'maintenance',
  },
  newFormOpen: false,
  machines: {},
  pageSize: 20,
  nextPage: 0,
  hasNextPage: true,
  count: 0,
  loading: false,
};
