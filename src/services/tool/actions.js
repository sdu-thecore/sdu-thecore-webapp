import { snakeCase } from 'lodash';
import {
  TOOL_READ_MANY,
  TOOL_READ_ONE,
  TOOL_CREATE_ONE,
  TOOL_UPDATE_ONE,
  TOOL_DELETE_ONE,
  REQUEST
} from '../../constants';
import { ENDPOINT_TOOLS, ENDPOINT_TOOL } from '../../endpoints';
import { walkKeys, walkValues, trim } from '../../util';

export const readTools = (sortBy = 'name') => (dispatch, getState) => {
  const { pageSize, nextPage } = getState().node;
  const offset = pageSize * nextPage;
  dispatch({
    type: TOOL_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_TOOLS({
          limit: pageSize,
          sortOrder: 'asc',
          populate: ['type', 'borrowed_by'].join(','),
          sortBy,
          offset
        })
      }
    }
  });
};

export const readTool = id => ({
  type: TOOL_READ_ONE + REQUEST,
  payload: {
    request: {
      url: ENDPOINT_TOOL(id)
    }
  },
  id
});

export const createTool = entity => ({
  type: TOOL_CREATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'POST',
      url: ENDPOINT_TOOLS(),
      data: walkKeys(snakeCase)(walkValues(trim)(entity))
    }
  },
  entity
});

export const updateTool = (id, entity) => ({
  type: TOOL_UPDATE_ONE + REQUEST,
  payload: {
    request: {
      method: 'PATCH',
      url: ENDPOINT_TOOL(id),
      data: walkKeys(snakeCase)(walkValues(trim)(entity))
    }
  },
  id,
  entity
});

export const deleteTool = id => ({
  type: TOOL_DELETE_ONE + REQUEST,
  payload: {
    request: {
      method: 'DELETE',
      url: ENDPOINT_TOOL(id)
    }
  },
  id
});
