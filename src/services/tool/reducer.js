import { camelCase } from 'lodash';
import defaultState from './state';
import { walkKeys, arrayToObject } from '../../util';
import {
  TOOL_READ_MANY,
  TOOL_READ_ONE,
  TOOL_CREATE_ONE,
  TOOL_UPDATE_ONE,
  TOOL_DELETE_ONE,
  REQUEST,
  SUCCESS,
  FAIL
} from '../../constants';

const reducer = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case TOOL_READ_ONE + REQUEST: {
      return {
        ...state,
        isReadingOne: true
      };
    }
    case TOOL_READ_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isReadingOne: false
      };
    }
    case TOOL_READ_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        const newEntities = arrayToObject(walkKeys(camelCase)([data]));
        const newCount =
          !state.entities[data.id] && !state.hasNextPage
            ? state.count + 1
            : state.count;
        const entities = { ...state.permissions, ...newEntities };
        return {
          ...state,
          isReadingOne: false,
          count: newCount,
          entities
        };
      }
      return {
        ...state,
        isReadingOne: false
      };
    }
    case TOOL_READ_MANY + REQUEST: {
      return {
        ...state,
        isReadingMany: true
      };
    }
    case TOOL_READ_MANY + REQUEST + FAIL: {
      return {
        ...state,
        isReadingMany: false
      };
    }
    case TOOL_READ_MANY + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data, count } = action.payload.data;
        const newEntities = arrayToObject(walkKeys(camelCase)(data));
        const entities = { ...state.permissions, ...newEntities };
        const currentCount = Object.keys(entities).length;
        const loadedCount = Object.keys(newEntities).length;
        const nextPage =
          loadedCount < state.pageSize ? state.nextPage : state.nextPage + 1;
        return {
          ...state,
          isReadingMany: false,
          hasNextPage: count > currentCount,
          nextPage: state.hasNextPage ? nextPage : 0,
          count,
          entities
        };
      }
      return {
        ...state,
        isReadingMany: false
      };
    }
    case TOOL_CREATE_ONE + REQUEST: {
      return {
        ...state,
        isCreatingOne: true
      };
    }
    case TOOL_CREATE_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isCreatingOne: false
      };
    }
    case TOOL_CREATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        const newEntities = arrayToObject(walkKeys(camelCase)([data]));
        const entities = { ...state.entities, ...newEntities };
        return {
          ...state,
          isCreatingOne: false,
          count: state.count + 1,
          hasNextPage: true,
          entities
        };
      }
      return {
        ...state,
        isCreatingOne: false
      };
    }
    case TOOL_DELETE_ONE + REQUEST: {
      return {
        ...state,
        isDeletingOne: true
      };
    }
    case TOOL_DELETE_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isDeletingOne: false
      };
    }
    case TOOL_DELETE_ONE + REQUEST + SUCCESS: {
      if (
        action.meta &&
        action.meta.previousAction &&
        action.meta.previousAction.id
      ) {
        const { previousAction } = action.meta;
        const entityIds = Object.keys(state.entities);
        const filteredIds = entityIds.filter(id => id !== previousAction.id);
        const entities = filteredIds.reduce(
          (obj, id) => ({
            ...obj,
            [id]: state.entities[id]
          }),
          {}
        );
        return {
          ...state,
          isDeletingOne: false,
          count: state.count - 1,
          entities
        };
      }
      return {
        ...state,
        isDeletingOne: false
      };
    }
    case TOOL_UPDATE_ONE + REQUEST: {
      return {
        ...state,
        isUpdatingOne: true
      };
    }
    case TOOL_UPDATE_ONE + REQUEST + FAIL: {
      return {
        ...state,
        isUpdatingOne: false
      };
    }
    case TOOL_UPDATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        const newEntities = arrayToObject(walkKeys(camelCase)([data]));
        const entities = { ...state.entities, ...newEntities };
        return {
          ...state,
          isUpdatingOne: false,
          hasNextPage: true,
          entities
        };
      }
      return {
        ...state,
        isUpdatingOne: false
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
