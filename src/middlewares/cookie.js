import cookieMiddleware, {
  getStateFromCookies
} from 'redux-cookies-middleware';

// configure information stored in cookies
const paths = {
  'auth.token': {
    name: 'auth.token'
  },
  'user.profile': {
    name: 'user.profile'
  }
};

const mergeState = (defaultState, currentState) => (state, key) => ({
  ...state,
  [key]: {
    ...defaultState[key],
    ...currentState[key]
  }
});

export const getInitialState = defaultState =>
  Object.keys(defaultState).reduce(
    mergeState(defaultState, getStateFromCookies({}, paths)),
    {}
  );

export default cookieMiddleware(paths);
