import { useRef, useEffect } from 'react';

// Created by the React god Dan Abramov himself.
// Reference: https://overreacted.io/making-setinterval-declarative-with-react-hooks/
export function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
    return null;
  }, [delay]);
}

// Run an action on first mount and then periodically.
export function useInitialAndPeriodicAction(callback, delay) {
  // Run action on the first mount.
  // Reference: https://reactjs.org/docs/hooks-reference.html#conditionally-firing-an-effect
  useEffect(() => {
    callback();
  }, [callback]);

  // Run action periodically.
  useInterval(callback, delay);
}
