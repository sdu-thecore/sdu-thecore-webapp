import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

function AuthZ(options = {}) {
  const config = { roles: [], redirect: false, redirectTarget: '', ...options };
  return InsecureComponent => {
    class SecureComponent extends React.Component {
      constructor() {
        super();
        this.isAuthorized = this.isAuthorized.bind(this);
      }

      componentDidMount() {
        this.redirect();
      }

      componentDidUpdate() {
        this.redirect();
      }

      redirect() {
        const { history } = this.props;
        if (config.redirect && !this.isAuthorized()) {
          if (config.redirectTarget) {
            history.replace(config.redirectTarget);
          } else {
            history.goBack();
          }
        }
      }

      isAuthorized() {
        const { token, profile } = this.props;
        if (!token) return false;
        if (!profile) return false;
        if (!profile.role) return false;
        if (!config.roles.includes(profile.role)) return false;
        return true;
      }

      render() {
        // use destructuring to prevent injected props from being passed on
        const { history, token, profile, ...rest } = this.props;
        if (!this.isAuthorized()) {
          return null;
        }
        // eslint-disable-next-line react/jsx-props-no-spreading
        return <InsecureComponent {...rest} />;
      }
    }

    SecureComponent.propTypes = {
      history: PropTypes.shape({
        goBack: PropTypes.func.isRequired,
        replace: PropTypes.func.isRequired
      }).isRequired,
      token: PropTypes.string.isRequired,
      profile: PropTypes.shape({
        role: PropTypes.string.isRequired
      })
    };

    SecureComponent.defaultProps = {
      profile: null
    };

    const mapStateToProps = ({ auth, user }) => ({
      profile: user.profile,
      token: auth.token
    });

    return connect(mapStateToProps)(withRouter(SecureComponent));
  };
}

export default AuthZ;
