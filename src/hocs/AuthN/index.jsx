import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

function AuthN(options = {}) {
  const config = { redirect: false, redirectTarget: '', ...options };
  return InsecureComponent => {
    class SecureComponent extends React.Component {
      constructor() {
        super();
        this.isAuthenticated = this.isAuthenticated.bind(this);
      }

      componentDidMount() {
        this.redirect();
      }

      componentDidUpdate() {
        this.redirect();
      }

      redirect() {
        const { history } = this.props;
        if (config.redirect && !this.isAuthenticated()) {
          if (config.redirectTarget) {
            history.replace(config.redirectTarget);
          } else {
            history.goBack();
          }
        }
      }

      isAuthenticated() {
        const { token } = this.props;
        if (!token) return false;
        return true;
      }

      render() {
        // use destructuring to prevent injected props from being passed on
        const { history, token, profile, ...rest } = this.props;
        if (!this.isAuthenticated()) {
          return null;
        }
        // eslint-disable-next-line react/jsx-props-no-spreading
        return <InsecureComponent {...rest} />;
      }
    }

    SecureComponent.propTypes = {
      history: PropTypes.shape({
        goBack: PropTypes.func.isRequired,
        replace: PropTypes.func.isRequired
      }).isRequired,
      token: PropTypes.string.isRequired,
      profile: PropTypes.shape({
        role: PropTypes.string.isRequired
      })
    };

    SecureComponent.defaultProps = {
      profile: null
    };

    const mapStateToProps = ({ auth }) => ({
      token: auth.token
    });

    return connect(mapStateToProps)(withRouter(SecureComponent));
  };
}

export default AuthN;
