export const objectToArray = obj => Object.keys(obj).map(key => obj[key]);

export const arrayToObject = array =>
  array.reduce((prev, curr) => {
    const next = prev;
    next[curr.id] = curr;
    return next;
  }, {});
