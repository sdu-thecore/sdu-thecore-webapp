import React from "react";
import { render } from "react-dom";
import App from "./scenes/App";

window.TAG = process.env.REACT_APP_TAG;
window.API_URL = process.env.REACT_APP_API_URL;
window.HUB_URL = process.env.REACT_APP_HUB_URL;

render(<App />, document.getElementById("root"));
