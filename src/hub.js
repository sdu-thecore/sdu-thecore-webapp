import socketio from 'socket.io-client';
import { HUB_URL } from './endpoints';

let socket = null;

export const connect = token => {
  if (!socket) {
    socket = socketio(HUB_URL, {
      transportOptions: {
        polling: {
          extraHeaders: {
            authorization: `Bearer ${token}`
          }
        }
      }
    });
  }

  return socket;
};

export const disconnect = () => {
  socket = null;
};
