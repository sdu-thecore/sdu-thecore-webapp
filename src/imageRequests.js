import axios from 'axios';
import { ENDPOINT_IMAGES, ENDPOINT_IMAGE } from './endpoints';

export const createImage = async (bearerToken, blob, filename) => {
  const extension = blob.type && blob.type.split(/\//g).pop();
  const filenameChunks = filename.split(/\./g);
  let newFilename;
  if (filenameChunks.length) {
    filenameChunks.pop();
    filenameChunks.push(extension);
    newFilename = filenameChunks.join('.');
  } else {
    newFilename = 'image';
  }
  const data = new FormData();
  data.append('image', blob, newFilename);
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${bearerToken}`
    }
  };
  return axios.post(ENDPOINT_IMAGES(), data, config);
};

export const deleteImage = async (bearerToken, id) => {
  const config = { headers: { Authorization: `Bearer ${bearerToken}` } };
  return axios.delete(ENDPOINT_IMAGE(id), config);
};
