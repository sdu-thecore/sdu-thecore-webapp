import { snakeCase } from 'lodash';

// use separate variable
// linter prefers destructuring
// destructuring does not work with the webpack environment plugin
const apiUrl = process.env.REACT_APP_API_URL;
const hubUrl = process.env.REACT_APP_HUB_URL;
const docsUrl = process.env.REACT_APP_DOCS_URL;
const apiVersion = 'v1';

export const API_URL = apiUrl;
export const HUB_URL = hubUrl;
export const DOCS_URL = docsUrl;

const withApiUrl = (endpoint) => `${apiUrl}/${apiVersion}${endpoint}`;

const withQueryString = (endpoint) => (params) => {
  if (!params) return endpoint;
  const query = Object.keys(params)
    .map((param) => `${snakeCase(param)}=${params[param]}`)
    .join('&');
  if (query) return `${withApiUrl(endpoint)}?${query}`;
  return withApiUrl(endpoint);
};

const withId = (endpoint, path = '') => (id) =>
  `${withApiUrl(endpoint)}/${id}${path}`;

const withNothing = (endpoint) => () => withApiUrl(endpoint);

// azure oauth2 authentification endpoints
export const ENDPOINT_OAUTH2_AZURE_AD_LOGIN = () =>
  `${apiUrl}/v1/auth/azure_ad`;
export const ENDPOINT_OAUTH2_AZURE_AD_LOGOUT = (redirectUri) => {
  const endpoint = 'https://login.microsoftonline.com/common/oauth2/logout';
  const search = `?post_logout_redirect_uri=${redirectUri}`;
  return redirectUri ? endpoint + search : endpoint;
};

// user endpoints
export const ENDPOINT_ME = withNothing('/me');
export const ENDPOINT_USERS = withQueryString('/users');
export const ENDPOINT_USER = withId('/users');

// issue endpoints
export const ENDPOINT_ISSUE = (id) => (id ? `/issues/${id}` : '/issues');

// application endpoints
export const ENDPOINT_APPLICATION = (app) =>
  withQueryString(`/applications/${app}`);

// node endpoints
export const ENDPOINT_NODES = withQueryString('/nodes');
export const ENDPOINT_NODE_SESSION = (nodeId, sessionId) =>
  `/nodes/${nodeId}/sessions/${sessionId}`;
export const ENDPOINT_NODE = withId('/nodes');

// permission endpoints
export const ENDPOINT_PERMISSIONS = withQueryString('/permissions');
export const ENDPOINT_PERMISSION = withId('/permissions');

// machine endpoints
export const ENDPOINT_MACHINES = withQueryString('/machines');
export const ENDPOINT_MACHINE = withId('/machines');
export const ENDPOINT_MACHINE_NEW = withQueryString('/machines/new');

// image endpoints
export const ENDPOINT_IMAGE = withId('/images');
export const ENDPOINT_IMAGES = withNothing('/images');
export const ENDPOINT_IMAGE_CONTENT = withId('/images', '/content');

// tool type endpoints
export const ENDPOINT_TOOL_TYPES = withQueryString('/tool_types');
export const ENDPOINT_TOOL_TYPE = withId('/tool_types');

// tool endpoints
export const ENDPOINT_TOOLS = withQueryString('/tools');
export const ENDPOINT_TOOL = withId('/tools');
