class Streamer {
  constructor(options = {}) {
    if (options.callback && typeof options.callback === 'function') {
      this.callback = options.callback;
    } else {
      this.callback = () => null;
      // Configure console logger during development.
      if (process.env.NODE_ENV === 'development') {
        // eslint-disable-next-line no-console
        this.callback = console.log;
      }
    }
    this.initialized = false;
    // Check if browser supports media source extensions (MSE).
    if (!('MediaSource' in window)) {
      this.callback(new Error('This browser does not support livestreaming.'));
      return;
    }
    // Check if video is a reference to a <video /> tag.
    if (!options.video || !(options.video instanceof HTMLVideoElement)) {
      this.callback(new Error('"options.video" is not a <video /> element.'));
      return;
    }
    // Check if socket is a socket.io client.
    if (!options.socket) {
      this.callback(
        new Error('"options.socket" is not an instance of socket.io.')
      );
      return;
    }
    // Check if event path is set to receive events.
    if (!options.eventPath || typeof options.eventPath !== 'string') {
      this.callback(
        new Error('"options.eventPath" has to be of type "string".')
      );
      return;
    }
    // Check if node ID was supplied.
    if (!options.nodeId || typeof options.nodeId !== 'string') {
      this.callback(new Error('"options.nodeId" has to be of type "string".'));
      return;
    }

    // Bind functions to current context to ensure correct context when used as event handlers.
    this.ensureStreamStart = this.ensureStreamStart.bind(this);
    this.onVideoError = this.onVideoError.bind(this);
    this.onVideoLoadedData = this.onVideoLoadedData.bind(this);
    this.onMediaSourceOpen = this.onMediaSourceOpen.bind(this);
    this.onSourceBufferError = this.onSourceBufferError.bind(this);
    this.onSourceBufferUpdateEnd = this.onSourceBufferUpdateEnd.bind(this);
    this.onSocketConnect = this.onSocketConnect.bind(this);
    this.onSocketDisconnect = this.onSocketDisconnect.bind(this);
    this.onSocketError = this.onSocketError.bind(this);
    this.onMime = this.onMime.bind(this);
    this.onInit = this.onInit.bind(this);
    this.onSegment = this.onSegment.bind(this);

    // Initialize member variables.
    this.socket = options.socket;
    this.video = options.video;
    this.eventPath = options.eventPath;
    this.nodeId = options.nodeId;
    this.mime = '';
    this.init = null;
    this.segment = null;
    this.mediaSource = null;
    this.sourceBuffer = null;
    this.streaming = false;
    this.watchdogTimer = null;
    this.initialized = true;
  }

  /* Public methods. */
  start() {
    // Do nothing if object could not be initialized correctly.
    if (!this.initialized) {
      this.callback(
        new Error('Streamer not initialized. Did you supply all options?')
      );
      return this;
    }

    // Attach socket events to start streaming.
    this.addSocketEvents();

    // Ensure stream is started.
    this.ensureStreamStart();

    return this;
  }

  stop() {
    // Do nothing if object could not be initialized correctly.
    if (!this.initialized) {
      this.callback(
        new Error('Streamer not initialized. Did you supply all options?')
      );
      return this;
    }

    // Update status.
    this.streaming = false;

    // Clean up video element.
    if (this.video) {
      // Remove event handlers to prevent memory leaks.
      this.removeVideoEvents();

      // Free memory by revoking object URL.
      URL.revokeObjectURL(this.video.src);
      // Pause video.
      this.video.pause();
      // Remove data source.
      this.video.removeAttribute('src');
      // Set video to loading state.
      this.video.load();
    }

    // Clean up socket connection.
    if (this.socket) {
      // Unsubscribe from video feed.
      this.socket.emit(`${this.eventPath}/stop`, this.nodeId);
      // Remove event handlers to prevent memory leaks.
      this.removeSocketEvents();
    }

    // Clean up media source.
    if (this.mediaSource) {
      // Remove event handlers to prevent memory leaks.
      this.removeMediaSourceEvents();
      // Check for any source buffers in media source.
      if (
        this.mediaSource.sourceBuffers &&
        this.mediaSource.sourceBuffers.length
      ) {
        // Remove source buffer from media source.
        this.mediaSource.removeSourceBuffer(this.sourceBuffer);
      }
      // Reset reference to media source.
      this.mediaSource = null;
    }

    // Clean up source buffer.
    if (this.sourceBuffer) {
      // Remove event handlers to prevent memory leaks.
      this.removeSourceBufferEvents();
      // Check if buffer is updating.
      if (this.sourceBuffer.updating) {
        // Abort update.
        this.sourceBuffer.abort();
      }
      // Reset reference to source buffer.
      this.sourceBuffer = null;
    }

    return this;
  }

  ensureStreamStart() {
    if (!this.streaming) {
      // Subscribe to video feed to start it if it is not already running.
      this.socket.emit(`${this.eventPath}/start`, this.nodeId);

      // Update status.
      this.streaming = true;

      // Clear watchdog timeout.
      clearTimeout(this.watchdogTimer);
      this.watchdogTimer = null;
    } else {
      this.watchdogTimer = setTimeout(this.ensureStreamStart, 500);
    }
  }

  /* Video methods */
  onVideoError(event) {
    // Pass error to callback function.
    this.callback(new Error(`Video error: ${event.type}`));
  }

  onVideoLoadedData() {
    // Start playing the video automatically.
    this.video.play();
  }

  addVideoEvents() {
    // Abort if video is not defined.
    if (!this.video) {
      return;
    }

    // Attach event listeners to handle video events.
    this.video.addEventListener('error', this.onVideoError, {
      capture: true,
      passive: true,
      once: true
    });
    this.video.addEventListener('loadeddata', this.onVideoLoadedData, {
      capture: true,
      passive: true,
      once: true
    });
  }

  removeVideoEvents() {
    // Abort if video is not defined.
    if (!this.video) {
      return;
    }

    // Remove event handlers to prevent memory leaks.
    this.video.removeEventListener('error', this.onVideoError, {
      capture: true,
      passive: true,
      once: true
    });
    this.video.removeEventListener('loadeddata', this.onVideoLoadedData, {
      capture: true,
      passive: true,
      once: true
    });
  }

  /* Media source methods */
  onMediaSourceOpen() {
    this.mediaSource.duration = Number.POSITIVE_INFINITY;
    this.sourceBuffer = this.mediaSource.addSourceBuffer(this.mime);
    this.sourceBuffer.mode = 'sequence';
    this.addSourceBufferEvents();
    this.sourceBuffer.appendBuffer(this.init);
    this.socket.addEventListener(`${this.eventPath}/segment`, this.onSegment, {
      capture: true,
      passive: true,
      once: false
    });

    // Mute video.
    this.video.muted = true;
  }

  addMediaSourceEvents() {
    // Abort if media source is not defined.
    if (!this.mediaSource) {
      return;
    }

    // Attach event listener to wait for media source to be open.
    this.mediaSource.addEventListener('sourceopen', this.onMediaSourceOpen, {
      capture: true,
      passive: true,
      once: true
    });
  }

  removeMediaSourceEvents() {
    // Abort if media source is not defined.
    if (!this.mediaSource) {
      return;
    }

    // Remove event listeners to prevent memory leaks.
    this.mediaSource.removeEventListener('sourceopen', this.onMediaSourceOpen, {
      capture: true,
      passive: true,
      once: true
    });
  }

  /* Source buffer methods */
  onSourceBufferError(event) {
    this.callback(new Error(`Source buffer error: ${event.type}`));
  }

  onSourceBufferUpdateEnd() {
    // Abort if source buffer is updating and can't be modified.
    if (this.sourceBuffer.updating) {
      return;
    }

    // Append pending segment.
    if (this.segment) {
      this.sourceBuffer.appendBuffer(this.segment);
      this.segment = null;
      return;
    }

    // Check if buffered media exists.
    if (!this.sourceBuffer.buffered.length) {
      return;
    }

    // Remove old media to reduce memory footprint.
    const { currentTime } = this.video;
    const start = this.sourceBuffer.buffered.start(0);
    const end = this.sourceBuffer.buffered.end(0);
    const past = currentTime - start;
    if (past > 20 && currentTime < end) {
      this.sourceBuffer.remove(start, currentTime - 4);
    }
  }

  addSourceBufferEvents() {
    // Ignore if source buffer is not initialized.
    if (!this.sourceBuffer) {
      return;
    }

    // Attach source buffer event handlers.
    this.sourceBuffer.addEventListener('error', this.onSourceBufferError, {
      capture: true,
      passive: true,
      once: true
    });
    this.sourceBuffer.addEventListener(
      'updateend',
      this.onSourceBufferUpdateEnd,
      { capture: true, passive: true, once: false }
    );
  }

  removeSourceBufferEvents() {
    // Ignore if source buffer is not initialized.
    if (!this.sourceBuffer) {
      return;
    }

    // Remove event handlers to prevent memory leaks.
    this.sourceBuffer.removeEventListener('error', this.onSourceBufferError, {
      capture: true,
      passive: true,
      once: true
    });
    this.sourceBuffer.removeEventListener(
      'updateend',
      this.onSourceBufferUpdateEnd,
      { capture: true, passive: true, once: false }
    );
  }

  /* Socket methods */
  onSocketConnect() {
    // Attach event handler to receive mime type.
    this.socket.addEventListener(`${this.eventPath}/mime`, this.onMime, {
      capture: true,
      passive: true,
      once: true
    });

    // Request mime type.
    this.socket.emit(`${this.eventPath}/mime`, this.nodeId);
  }

  onSocketDisconnect(reason) {
    // Store if it was streaming prior to disconnect.
    const wasStreaming = this.streaming;

    // Stop and reset class.
    this.stop();

    if (wasStreaming) {
      // Restart if streaming was interrupted.
      this.start();
    }

    // Pass error to callback function.
    this.callback(new Error(`Socket disconnected: "${reason}"`));
  }

  onSocketError(error) {
    // Store if it was streaming prior to error.
    const wasStreaming = this.streaming;

    // Stop and reset class.
    this.stop();

    if (wasStreaming) {
      // Restart if streaming was interrupted.
      this.start();
    }

    // Pass error to callback function.
    this.callback(new Error(`Socket error: "${error}"`));
  }

  onMime(mime) {
    // Set mime type if it is supported.
    this.mime = mime;

    // Check if MIME type is supported by MSE.
    if (!MediaSource.isTypeSupported(this.mime)) {
      this.callback(new Error(`Unsupported MIME type: ${this.mime}`));
      return;
    }

    // Attach event listeners to receive initialization segment.
    this.socket.addEventListener(
      `${this.eventPath}/initialization`,
      this.onInit,
      {
        capture: true,
        passive: true,
        once: true
      }
    );

    // Request initialization segment.
    this.socket.emit(`${this.eventPath}/initialization`, this.nodeId);
  }

  onInit(arrayBuffer) {
    this.init = arrayBuffer;
    this.mediaSource = new MediaSource();
    this.addMediaSourceEvents();
    this.addVideoEvents();
    this.video.src = URL.createObjectURL(this.mediaSource);
  }

  onSegment(arrayBuffer) {
    if (this.sourceBuffer) {
      if (this.sourceBuffer.buffered.length) {
        // Skip segments to reduce delay from source to client.
        const lag = this.sourceBuffer.buffered.end(0) - this.video.currentTime;
        if (lag > 0.5) {
          this.video.currentTime = this.sourceBuffer.buffered.end(0) - 0.5;
        }
      }
      // Buffer segment if source buffer is busy.
      if (this.sourceBuffer.updating) {
        this.segment = arrayBuffer;
      } else {
        // Skip old segment.
        this.segment = null;
        // Append new segment.
        this.sourceBuffer.appendBuffer(arrayBuffer);
      }
    }
  }

  addSocketEvents() {
    // Do nothing if socket has already been reset or does not exist.
    if (!this.socket) {
      return;
    }

    // Attach event listeners.
    this.socket.addEventListener('connect', this.onSocketConnect, {
      capture: true,
      passive: true,
      once: true
    });
    this.socket.addEventListener('disconnect', this.onSocketDisconnect, {
      capture: true,
      passive: true,
      once: true
    });
    this.socket.addEventListener('error', this.onSocketError, {
      capture: true,
      passive: true,
      once: true
    });
  }

  removeSocketEvents() {
    // Do nothing if socket has already been reset or does not exist.
    if (!this.socket) {
      return;
    }

    // Remove event handlers to prevent memory leaks.
    this.socket.removeEventListener('connect', this.onSocketConnect, {
      capture: true,
      passive: true,
      once: true
    });
    this.socket.removeEventListener('disconnect', this.onSocketDisconnect, {
      capture: true,
      passive: true,
      once: true
    });
    this.socket.removeEventListener('error', this.onSocketError, {
      capture: true,
      passive: true,
      once: true
    });
    this.socket.removeEventListener(`${this.eventPath}/mime`, this.onMime, {
      capture: true,
      passive: true,
      once: true
    });
    this.socket.removeEventListener(
      `${this.eventPath}/initialization`,
      this.onInit,
      {
        capture: true,
        passive: true,
        once: true
      }
    );
    this.socket.removeEventListener(
      `${this.eventPath}/segment`,
      this.onSegment,
      {
        capture: true,
        passive: true,
        once: false
      }
    );
  }
}

export default Streamer;
