import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect, disconnect } from '../../hub';
import Streamer from './Streamer';

const Livestream = ({ nodeId, token }) => {
  const videoRef = useRef(null);
  useEffect(() => {
    let streamer = null;
    let socket = null;

    if (token) {
      socket = connect(token);

      streamer = new Streamer({
        eventPath: '/nodes/capabilities/raspberry_pi_camera',
        callback: () => null,
        video: videoRef.current,
        nodeId,
        socket
      });

      streamer.start();
    }

    return () => {
      if (streamer) {
        streamer.stop();
      }
      if (socket) {
        disconnect();
      }
    };
  }, [token, nodeId]);

  return (
    <video
      style={{ width: '100%', height: 'auto' }}
      muted
      ref={videoRef}
      autoPlay
    />
  );
};

Livestream.propTypes = {
  nodeId: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired
};

export default Livestream;
