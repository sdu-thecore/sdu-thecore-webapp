const styles = theme => ({
  button: {
    marginRight: theme.spacing(2)
  },
  buttonWhite: {
    backgroundColor: theme.palette.primary.contrastText,
    color: theme.palette.primary.main
  }
});

export default styles;
