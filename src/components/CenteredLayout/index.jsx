import React from 'react';
import PropTypes from 'prop-types';
import { Grid, withStyles } from '@material-ui/core';
import styles from './styles';

const CenteredLayout = ({ children, classes }) => (
  <Grid
    container
    className={classes.container}
    direction="row"
    alignItems="center"
    justify="center"
    spacing={2}
  >
    {children}
  </Grid>
);

CenteredLayout.propTypes = {
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired
  }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default withStyles(styles)(CenteredLayout);
