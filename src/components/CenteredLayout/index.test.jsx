import React from 'react';
import { render } from '@testing-library/react';
import CenteredLayout from '.';

describe('CenteredLayout', () => {
  it('renders child text', () => {
    // arrange
    const text = 'This is a test!';

    // act
    const { getByText, container } = render(
      <CenteredLayout>{text}</CenteredLayout>
    );

    // assert
    expect(getByText(text)).not.toBeNull();
    expect(container).toMatchSnapshot();
  });
});
