import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  LinearProgress,
  Typography,
  withStyles,
} from '@material-ui/core';
import { v4 } from 'uuid';
import clsx from 'clsx';
import Cropper from 'react-easy-crop';
import FileInputButton from '../FileInputButton/FileInputButton';
import {
  fileToDataUrl,
  dataUrlToCroppedBlobAndDataUrl,
  roundTo,
} from './helpers';

const styles = (theme) => ({
  cropperContainer: {
    position: 'relative',
    height: 300,
  },
  dialogTitleBusy: {
    paddingTop: theme.spacing(3) - 4,
  },
  img: {
    width: '100%',
  },
});

class ImageUpload extends Component {
  constructor() {
    super();
    this.uuid = v4();
    this.closeDialog = this.closeDialog.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.setCrop = this.setCrop.bind(this);
    this.setZoom = this.setZoom.bind(this);
    this.selectFile = this.selectFile.bind(this);
    this.setCalculatedCrop = this.setCalculatedCrop.bind(this);
    this.cropImage = this.cropImage.bind(this);
  }

  setCrop(crop) {
    const { onChange } = this.props;
    onChange({ crop });
  }

  setZoom(zoom) {
    const { onChange } = this.props;
    onChange({ zoom });
  }

  setCalculatedCrop(croppedArea, croppedAreaPixels) {
    const { onChange } = this.props;
    onChange({ croppedArea, croppedAreaPixels });
  }

  async selectFile(evt) {
    const { onChange } = this.props;
    if (evt.target.files && evt.target.files.length > 0) {
      const file = evt.target.files[0];
      const originalDataUrl = await fileToDataUrl(file);
      onChange({
        filename: file.name,
        originalDataUrl,
        zoom: 1,
        crop: { x: 0, y: 0 },
      });
    }
  }

  cropImage() {
    const { onChange, value } = this.props;
    const { originalDataUrl, croppedAreaPixels } = value;
    onChange({ isBusyCropping: true }, async () => {
      const result = await dataUrlToCroppedBlobAndDataUrl(
        originalDataUrl,
        croppedAreaPixels
      );

      onChange({
        blob: result.blob,
        croppedDataUrl: result.dataUrl,
        isDialogOpen: false,
        isBusyCropping: false,
      });
    });
  }

  openDialog() {
    const { onChange } = this.props;
    onChange({ isDialogOpen: true });
  }

  closeDialog() {
    const { onChange } = this.props;
    onChange({ isDialogOpen: false });
  }

  render() {
    const {
      ButtonProps,
      label,
      required,
      aspectRatio,
      classes,
      value,
    } = this.props;
    const {
      isDialogOpen,
      isBusyCropping,
      zoom,
      crop,
      croppedDataUrl,
      originalDataUrl,
      blob,
    } = value;
    const megabyte = 10e6;
    const labelText = required ? `${label} *` : label;
    return (
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
          {croppedDataUrl ? (
            <img
              className={classes.img}
              src={croppedDataUrl}
              alt="Cropped permission thumbnail"
            />
          ) : (
            <Typography>No preview available.</Typography>
          )}
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
          <Grid container spacing={2} direction="column">
            <Grid item xs={12}>
              {/* eslint-disable react/jsx-props-no-spreading */}
              <Button required {...ButtonProps} onClick={this.openDialog}>
                {labelText}
              </Button>
              {/* eslint-enable react/jsx-props-no-spreading */}
              <Dialog
                open={isDialogOpen}
                onClose={this.closeDialog}
                scroll="body"
                aria-labelledby={`ImageUpload_Title_${this.uuid}`}
              >
                {isBusyCropping && <LinearProgress color="primary" />}
                <DialogTitle
                  className={clsx({
                    [classes.dialogTitleBusy]: isBusyCropping,
                  })}
                  id={`ImageUpload_Title_${this.uuid}`}
                >
                  Upload and crop image
                </DialogTitle>
                <DialogContent>
                  <FileInputButton
                    accept="image/*"
                    color="primary"
                    variant="contained"
                    onChange={this.selectFile}
                  >
                    Select image
                  </FileInputButton>
                </DialogContent>
                <DialogContent>
                  {originalDataUrl ? (
                    <div className={classes.cropperContainer}>
                      <Cropper
                        minZoom={0.4}
                        key={originalDataUrl}
                        image={originalDataUrl}
                        zoomSpeed={0.2}
                        crop={crop}
                        zoom={zoom}
                        aspect={aspectRatio}
                        restrictPosition={zoom >= 1}
                        onCropChange={this.setCrop}
                        onZoomChange={this.setZoom}
                        onCropComplete={this.setCalculatedCrop}
                      />
                    </div>
                  ) : (
                    <DialogContentText>
                      No preview available. Select an image to proceed.
                    </DialogContentText>
                  )}
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.closeDialog}>Cancel</Button>
                  <Button
                    onClick={this.cropImage}
                    disabled={!originalDataUrl}
                    color="primary"
                  >
                    OK
                  </Button>
                </DialogActions>
              </Dialog>
            </Grid>
            {blob && blob.size && (
              <Grid item xs={12}>
                <Typography variant="subtitle2" inline>
                  Filesize:{' '}
                </Typography>
                <Typography inline>
                  {roundTo(blob.size / megabyte, 2)}MB
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

ImageUpload.propTypes = {
  aspectRatio: PropTypes.number,
  label: PropTypes.string,
  name: PropTypes.string,
  required: PropTypes.bool,
  ButtonProps: PropTypes.shape({}),
  classes: PropTypes.shape({
    cropperContainer: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    dialogTitleBusy: PropTypes.string.isRequired,
  }).isRequired,
  value: PropTypes.shape({
    isDialogOpen: PropTypes.bool.isRequired,
    isBusyCropping: PropTypes.bool.isRequired,
    zoom: PropTypes.number.isRequired,
    crop: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired,
    }).isRequired,
    croppedArea: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired,
      width: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired,
    }).isRequired,
    croppedAreaPixels: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired,
      width: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired,
    }).isRequired,
    blob: PropTypes.instanceOf(Blob),
    filename: PropTypes.string.isRequired,
    croppedDataUrl: PropTypes.string.isRequired,
    originalDataUrl: PropTypes.string.isRequired,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
};

ImageUpload.defaultProps = {
  aspectRatio: 1,
  label: 'Upload image',
  name: 'image',
  required: false,
  ButtonProps: {},
};

export default withStyles(styles)(ImageUpload);
