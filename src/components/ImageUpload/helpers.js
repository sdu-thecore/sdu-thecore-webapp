// convert a file object to a data URL
export const fileToDataUrl = file => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => resolve(reader.result));
    reader.addEventListener('error', error => reject(error));
    reader.readAsDataURL(file);
  });
};

// create an image object from a data URL
const dataUrlToImage = url => {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.addEventListener('load', () => resolve(image));
    image.addEventListener('error', error => reject(error));
    image.src = url;
  });
};

// crops the file from a data URL to the given dimensions and returns a blob and a data URL
export const dataUrlToCroppedBlobAndDataUrl = (dataUrl, croppedAreaPixels) => {
  return new Promise((resolve, reject) => {
    dataUrlToImage(dataUrl)
      .then(image => {
        const canvas = document.createElement('canvas');
        canvas.width = croppedAreaPixels.width;
        canvas.height = croppedAreaPixels.height;
        const ctx = canvas.getContext('2d');

        ctx.fillStyle = '#ffffff';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(
          image,
          croppedAreaPixels.x,
          croppedAreaPixels.y,
          croppedAreaPixels.width,
          croppedAreaPixels.height,
          0,
          0,
          croppedAreaPixels.width,
          croppedAreaPixels.height
        );

        const mimeType = 'image/jpeg';
        canvas.toBlob(blob => {
          const croppedDataUrl = canvas.toDataURL(mimeType, 1);
          resolve({ blob, dataUrl: croppedDataUrl });
        }, mimeType);
      })
      .catch(reject);
  });
};

// rounds a number to the specified amount of digits
export const roundTo = (num, digits) =>
  Math.round(num * 10 ** digits) / 10 ** digits;
