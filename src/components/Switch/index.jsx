import React from 'react';
import PropTypes from 'prop-types';
import { Switch as MuiSwitch, FormControlLabel } from '@material-ui/core';

const TextField = ({ name, inputProps, label, onChange, ...rest }) => {
  /* eslint-disable react/jsx-props-no-spreading */
  const extendedInputProps = name ? { ...inputProps, name } : inputProps;
  return label ? (
    <FormControlLabel
      onChange={onChange}
      control={<MuiSwitch inputProps={extendedInputProps} {...rest} />}
      label={label}
    />
  ) : (
    <MuiSwitch inputProps={extendedInputProps} onChange={onChange} {...rest} />
  );
  /* eslint-enable react/jsx-props-no-spreading */
};

TextField.propTypes = {
  name: PropTypes.string,
  label: PropTypes.node,
  inputProps: PropTypes.shape({}),
  onChange: PropTypes.func
};

TextField.defaultProps = {
  name: '',
  label: '',
  inputProps: null,
  onChange: () => null
};

export default TextField;
