import React from 'react';
import PropTypes from 'prop-types';
import { Button, withStyles } from '@material-ui/core';
import { v4 } from 'uuid';

const styles = {
  input: {
    display: 'none',
  },
};

const FileInputButton = ({
  accept,
  id,
  multiple,
  classes,
  name,
  onChange,
  ...rest
}) => (
  <>
    <input
      className={classes.input}
      multiple={multiple}
      onChange={onChange}
      accept={accept}
      name={name}
      id={id}
      type="file"
    />
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <Button htmlFor={id} component="label" {...rest} />
  </>
);

FileInputButton.propTypes = {
  accept: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  multiple: PropTypes.bool,
  classes: PropTypes.shape({
    input: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

FileInputButton.defaultProps = {
  accept: '*',
  id: v4(),
  onChange: () => null,
  name: '',
  multiple: false,
};

export default withStyles(styles)(FileInputButton);
