import React from 'react';
import PropTypes from 'prop-types';
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  FormLabel
} from '@material-ui/core';

const CheckboxSelection = ({
  options,
  disabled,
  selected,
  helperText,
  transformLabels,
  onChange
}) => (
  <FormControl component="fieldset">
    {helperText && <FormLabel component="legend">{helperText}</FormLabel>}
    <FormGroup>
      {options.length ? (
        options.map(option => (
          <FormControlLabel
            key={option}
            control={
              <Checkbox
                color="primary"
                disabled={disabled}
                checked={selected.includes(option)}
                inputProps={{ name: option }}
                onChange={onChange}
                value={option}
              />
            }
            label={transformLabels(option)}
          />
        ))
      ) : (
        <FormHelperText>No options available.</FormHelperText>
      )}
    </FormGroup>
  </FormControl>
);

CheckboxSelection.propTypes = {
  options: PropTypes.arrayOf(PropTypes.string),
  selected: PropTypes.arrayOf(PropTypes.string),
  disabled: PropTypes.bool,
  helperText: PropTypes.string,
  transformLabels: PropTypes.func,
  onChange: PropTypes.func
};

CheckboxSelection.defaultProps = {
  options: [],
  selected: [],
  helperText: '',
  transformLabels: option => option,
  onChange: undefined,
  disabled: false
};

export default CheckboxSelection;
