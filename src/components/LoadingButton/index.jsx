import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, CircularProgress, Button } from '@material-ui/core';
import styles from './styles';

/* eslint-disable react/jsx-props-no-spreading */
const LoadingButton = ({
  loading,
  progressColor,
  className,
  progressClasses,
  children,
  classes,
  ...rest
}) => (
  <Button className={className} {...rest}>
    {children}
    {loading && (
      <CircularProgress
        className={classes.progress}
        size={24}
        color={progressColor}
      />
    )}
  </Button>
);
/* eslint-enable react/jsx-props-no-spreading */

LoadingButton.propTypes = {
  className: PropTypes.string,
  progressClasses: PropTypes.string,
  progressColor: PropTypes.oneOf(['inherit', 'primary', 'secondary']),
  loading: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

LoadingButton.defaultProps = {
  className: '',
  loading: false,
  progressColor: 'inherit',
  children: null,
};

export default withStyles(styles)(LoadingButton);
