import { Component } from 'react';
import PropTypes from 'prop-types';

class TimeAgo extends Component {
  constructor(props) {
    super(props);
    const { date } = this.props;
    const delta = new Date().getTime() - new Date(date).getTime();
    this.timer = null;
    this.state = { delta };
    this.updateInterval = this.updateInterval.bind(this);
  }

  componentDidMount() {
    if (!this.timer) {
      this.timer = setInterval(this.updateInterval.bind(this), 1000);
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  updateInterval() {
    const { date } = this.props;
    this.setState({
      delta: new Date().getTime() - new Date(date).getTime()
    });
  }

  render() {
    const { delta } = this.state;
    const sec = 1000;
    const min = 60;
    const hour = 60;
    const day = 24;
    const month = 30;
    const year = 12;
    let interval = sec;
    // generate human readable time interval
    interval *= min;
    if (interval > delta) {
      return 'Just now';
    }
    interval *= hour;
    if (interval > delta) {
      const elapsed = Math.floor(delta / (interval / hour));
      return `${elapsed} ${elapsed > 1 ? 'minutes' : 'minute'} ago`;
    }
    interval *= day;
    if (interval > delta) {
      const elapsed = Math.floor(delta / (interval / day));
      return `${elapsed} ${elapsed > 1 ? 'hours' : 'hour'} ago`;
    }
    interval *= month;
    if (interval > delta) {
      const elapsed = Math.floor(delta / (interval / month));
      return `${elapsed} ${elapsed > 1 ? 'days' : 'day'} ago`;
    }
    interval *= year;
    if (interval > delta) {
      const elapsed = Math.floor(delta / (interval / year));
      return `${elapsed} ${elapsed > 1 ? 'months' : 'month'} ago`;
    }
    const elapsed = Math.floor(delta / interval);
    return `${elapsed} ${elapsed > 1 ? 'years' : 'year'} ago`;
  }
}

TimeAgo.propTypes = {
  date: PropTypes.string.isRequired
};

export default TimeAgo;
