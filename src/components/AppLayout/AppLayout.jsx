import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import queryString from 'query-string';
import AppDrawer from './AppDrawer';
import AppMenu from './AppMenu';
import { readMe } from '../../services/user/actions';
import { logout, login } from '../../services/auth/actions';
import { ENDPOINT_OAUTH2_AZURE_AD_LOGOUT } from '../../endpoints';

const adaptMixin = (mixin, oldProp, newProp) => {
  const adaptedStyles = {};
  Object.keys(mixin).forEach((prop) => {
    if (prop === oldProp) {
      adaptedStyles[newProp] = mixin[oldProp];
    } else if (prop.charAt(0) === '@') {
      Object.keys(mixin[prop]).forEach((subprop) => {
        if (subprop === oldProp) {
          adaptedStyles[prop] = {
            [newProp]: mixin[prop][oldProp],
          };
        }
      });
    }
  });
  return adaptedStyles;
};

const styles = (theme) => ({
  layout: {
    flexGrow: 1,
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  main: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(2),
    overflow: 'auto',
    overflowY: 'scroll',
    ...adaptMixin(theme.mixins.toolbar, 'minHeight', 'marginTop'),
  },
  iframe: {
    display: 'none',
  },
});

class AppLayout extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
    };
    this.toggle = this.toggle.bind(this);
    this.close = this.close.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    this.login();
    this.getMeIfAuthorized();
  }

  componentDidUpdate() {
    this.login();
    this.getMeIfAuthorized();
  }

  getMeIfAuthorized() {
    const { auth, user, dispatchReadMe } = this.props;
    if (auth.token && !user.profile) {
      dispatchReadMe();
    }
  }

  login() {
    const { dispatchLogin, history } = this.props;
    const { location, replace } = history;
    if (location.hash.length > 1) {
      const { token } = queryString.parse(location.hash);
      dispatchLogin(token);
      replace(location.pathname);
    }
  }

  logout() {
    const { dispatchLogout } = this.props;
    dispatchLogout();
    this.setState((state) => ({
      ...state,
      logoutFromAzure: true,
    }));
  }

  toggle(value) {
    this.setState((state) => ({
      ...state,
      open: !state.open,
    }));
  }

  close() {
    this.setState((state) => ({
      ...state,
      open: false,
    }));
  }

  render() {
    const { open } = this.state;
    const { children, classes, user } = this.props;
    return (
      <div className={classes.layout}>
        <AppMenu
          user={user.profile}
          open={open}
          onLogout={this.logout}
          toggle={this.toggle}
        />
        <AppDrawer
          user={user.profile}
          open={open}
          toggle={this.toggle}
          onClose={this.close}
        />
        <main className={classes.main}>{children}</main>
      </div>
    );
  }
}

AppLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  classes: PropTypes.shape({
    layout: PropTypes.string.isRequired,
    main: PropTypes.string.isRequired,
    iframe: PropTypes.string.isRequired,
  }).isRequired,
  dispatchReadMe: PropTypes.func.isRequired,
  dispatchLogin: PropTypes.func.isRequired,
  dispatchLogout: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    token: PropTypes.string,
    logoutFromAzureAd: PropTypes.bool.isRequired,
  }).isRequired,
  user: PropTypes.shape({
    profile: PropTypes.shape({
      email: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      role: PropTypes.string.isRequired,
      azureId: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
    location: PropTypes.shape({
      hash: PropTypes.string.isRequired,
      pathname: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const mapStateToProps = ({ auth, user }) => ({ auth, user });
const mapDispatchToProps = (dispatch) => ({
  dispatchReadMe: () => dispatch(readMe()),
  dispatchLogout: () => dispatch(logout()),
  dispatchLogin: (token) => dispatch(login(token)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AppLayout))
);
