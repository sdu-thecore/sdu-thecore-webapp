import React from 'react';
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
  withStyles,
} from '@material-ui/core';
import { Menu, Help } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import AuthButton from './AuthButton';
import { useIsMobile } from './hooks';

const styles = (theme) => ({
  appBarDefault: {
    zIndex: theme.zIndex.drawer + 1,
  },
  buttonMenuDefault: {
    marginLeft: -theme.spacing(1.5),
    marginRight: theme.spacing(4),
  },
  buttonName: {
    marginRight: theme.spacing(2),
  },
  typography: {
    flexGrow: 1,
  },
});

const AppMenu = ({ classes, open, toggle, user }) => {
  const isMobile = useIsMobile();
  /* eslint-disable react/jsx-props-no-spreading */
  const LinkComponent = React.forwardRef((props, ref) => (
    <Link {...props} ref={ref} />
  ));
  /* eslint-enable react/jsx-props-no-spreading */
  return (
    <AppBar position="absolute" className={classes.appBarDefault}>
      <Toolbar>
        {isMobile ? (
          <IconButton
            className={classes.buttonMenuDefault}
            color="inherit"
            aria-label="Toggle drawer"
            onClick={toggle}
          >
            <Menu />
          </IconButton>
        ) : null}
        {!isMobile ? (
          <Typography
            className={classes.typography}
            variant="h6"
            color="inherit"
          >
            SDU - The Core
          </Typography>
        ) : (
          <div className={classes.typography}></div>
        )}
        {user && !isMobile && (
          <Button
            component={LinkComponent}
            className={classes.buttonName}
            to="/profile"
            color="inherit"
            aria-label="Open profile page"
          >
            {user.firstName.split(' ').shift()} {user.lastName}
          </Button>
        )}
        <AuthButton color="white" />
        <IconButton
          component={LinkComponent}
          to="/help"
          color="inherit"
          aria-label="Open help page"
        >
          <Help />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

AppMenu.defaultProps = {
  user: null,
};

export default withStyles(styles)(AppMenu);
