import React from 'react';
import PropTypes from 'prop-types';
import { MenuItem, TextField } from '@material-ui/core';

/* eslint-disable react/jsx-props-no-spreading */
const Select = ({ options, required, onClick, ...props }) => {
  const handleClick = (e) => {
    e.stopPropagation();
  };
  return (
    <TextField {...props} onClick={handleClick} required={required} select>
      {!required && (
        <MenuItem key="" value="">
          <i>No selection</i>
        </MenuItem>
      )}
      {options.map((option) => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  );
};
/* eslint-enable react/jsx-props-no-spreading */

Select.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }).isRequired
  ),
  required: PropTypes.bool,
};

Select.defaultProps = {
  options: [],
  required: false,
};

export default Select;
