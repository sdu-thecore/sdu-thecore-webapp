import React from 'react';
import PropTypes from 'prop-types';
import { LinearProgress } from '@material-ui/core';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { FixedLayout, Livestream } from '../../components';

const MachineCameraStream = ({ match, token }) => {
  return (
    <FixedLayout>
      <LinearProgress color="primary" />
      <Livestream token={token} nodeId={match.params.nodeId} />
    </FixedLayout>
  );
};

MachineCameraStream.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      nodeId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  token: PropTypes.string.isRequired,
};

const mapStateToProps = ({ auth }) => ({
  token: get(auth, 'token', ''),
});

export default connect(mapStateToProps)(MachineCameraStream);
