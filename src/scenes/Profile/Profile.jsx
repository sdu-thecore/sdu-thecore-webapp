import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  LinearProgress,
  ListItemText,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { connect } from 'react-redux';
import { startCase } from 'lodash';
import { POLL_INTERVAL } from '../../constants';
import { FixedLayout } from '../../components';
import { readMe } from '../../services/user/actions';
import { AuthN } from '../../hocs';
import PermissionPreview from './PermissionPreview';

class Profile extends Component {
  constructor() {
    super();
    this.timer = null;
  }

  componentDidMount() {
    const { dispatchReadMe } = this.props;
    if (!this.timer) {
      this.timer = setInterval(dispatchReadMe, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  render() {
    const { profile } = this.props;
    return (
      <FixedLayout>
        <Card>
          {!profile && <LinearProgress color="secondary" />}
          <CardHeader title="Profile" subheader="Your user information" />
          <CardContent>
            {profile ? (
              <Grid spacing={4} container>
                <Grid xs={12} md={6} lg={3} item>
                  <ListItemText
                    primary="Display name"
                    secondary={profile.displayName}
                  />
                </Grid>
                <Grid xs={12} md={6} lg={3} item>
                  <ListItemText primary="Email" secondary={profile.email} />
                </Grid>
                <Grid xs={12} md={6} lg={3} item>
                  <ListItemText
                    primary="Role"
                    secondary={startCase(profile.role)}
                  />
                </Grid>
                <Grid xs={12} md={6} lg={3} item>
                  <ListItemText
                    primary="Card number"
                    secondary={
                      profile.cardNumber
                        ? profile.cardNumber.substring(
                            profile.cardNumber.length - 6
                          )
                        : 'unknown'
                    }
                  />
                </Grid>
                <Grid xs={12} item>
                  <Paper>
                    <Table padding="default" size="small">
                      <TableHead>
                        <TableRow>
                          <TableCell>Image</TableCell>
                          <TableCell>Abbreviation</TableCell>
                          <TableCell>Name</TableCell>
                          <TableCell>Default permission</TableCell>
                        </TableRow>
                      </TableHead>
                      {profile.permissions.length ? (
                        <TableBody>
                          {profile.permissions.map((permissionId) => (
                            <PermissionPreview
                              key={permissionId}
                              permissionId={permissionId}
                            />
                          ))}
                        </TableBody>
                      ) : (
                        <TableBody>
                          <TableRow>
                            <TableCell colSpan={4}>
                              <b>
                                Currently, you don&apos;t have any permissions.
                              </b>
                            </TableCell>
                          </TableRow>
                        </TableBody>
                      )}
                    </Table>
                  </Paper>
                </Grid>
              </Grid>
            ) : (
              <Typography>
                Your profile information is currently unavailable.
              </Typography>
            )}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Profile.propTypes = {
  profile: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    cardNumber: PropTypes.string.isRequired,
    permissions: PropTypes.arrayOf(PropTypes.string),
  }),
  dispatchReadMe: PropTypes.func.isRequired,
};

Profile.defaultProps = {
  profile: null,
};

const mapStateToProps = ({ user }) => ({ profile: user.profile });
const mapDispatchToProps = (dispatch) => ({
  dispatchReadMe: () => dispatch(readMe()),
});

export default AuthN({
  redirect: true,
  redirectTarget: '/dashboard',
})(connect(mapStateToProps, mapDispatchToProps)(Profile));
