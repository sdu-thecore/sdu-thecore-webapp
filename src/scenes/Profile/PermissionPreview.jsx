import React, { useEffect, useState, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import { TableCell, TableRow, withStyles } from '@material-ui/core';
import { NotInterested, Check, Close } from '@material-ui/icons';
import axios from 'axios';
import { get } from 'lodash';
import { ENDPOINT_PERMISSION, ENDPOINT_IMAGE_CONTENT } from '../../endpoints';

const styles = (theme) => ({
  thumbnail: {
    width: theme.spacing(8),
  },
});

const PermissionPreview = ({ permissionId, classes }) => {
  const [state, setState] = useState({
    loading: false,
    permission: null,
  });
  let retryTimeout = useRef();
  const loadPermission = useCallback(async () => {
    setState((prevState) => ({ ...prevState, loading: true }));
    const res = await axios.get(ENDPOINT_PERMISSION(permissionId));
    const data = get(res, 'data.data', null);

    if (data) {
      setState({ permission: data, loading: false });
    } else {
      setState((prevState) => ({ ...prevState, loading: false }));
      retryTimeout.current = setTimeout(loadPermission, 1000);
    }
  }, [permissionId]);
  useEffect(() => {
    if (permissionId && !state.permission) {
      loadPermission(permissionId);
    }

    return () => {
      if (retryTimeout) {
        clearTimeout(retryTimeout.current);
      }
    };
  }, [permissionId, loadPermission, retryTimeout, state.permission]);
  return (
    <TableRow>
      {state.permission ? (
        <>
          <TableCell>
            {state.permission.image ? (
              <img
                className={classes.thumbnail}
                src={ENDPOINT_IMAGE_CONTENT(state.permission.image)}
                alt={`${state.permission.name}`}
              />
            ) : (
              <NotInterested className={classes.thumbnail} fontSize="large" />
            )}
          </TableCell>
          <TableCell>{state.permission.abbreviation}</TableCell>
          <TableCell>{state.permission.name}</TableCell>
          <TableCell>
            {state.permission.default ? <Check /> : <Close />}
          </TableCell>
        </>
      ) : (
        <TableCell colSpan={4}>Loading ...</TableCell>
      )}
    </TableRow>
  );
};

PermissionPreview.propTypes = {
  permissionId: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    thumbnail: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(PermissionPreview);
