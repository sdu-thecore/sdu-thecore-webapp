import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  TextField,
  InputAdornment,
  withStyles,
  Button
} from '@material-ui/core';
import { FixedLayout, FileInputButton, Select } from '../../components';
import Booking from './components/Booking';
import styles from './styles';

const mockJobs = [
  {
    id: 'a',
    created: '10/12/2018 12:01',
    start: '11/12/2018 12:00',
    end: '11/12/2018 15:00',
    time: '3',
    machine: {
      displayName: 'SLA-1'
    }
  },
  {
    id: 'b',
    created: '09/12/2018 16:25',
    start: '11/12/2018 17:00',
    end: '11/12/2018 18:30',
    time: '2',
    machine: {
      displayName: 'DLP-3'
    }
  },
  {
    id: 'c',
    created: '06/12/2018 09:30',
    start: '12/12/2018 13:00',
    end: '12/12/2018 18:00',
    time: '5',
    machine: {
      displayName: 'FDM-5'
    }
  },
  {
    id: 'd',
    created: '08/12/2018 18:56',
    start: '16/12/2018 07:00',
    end: '16/12/2018 17:00',
    time: '10',
    machine: {
      displayName: 'SLA-2'
    }
  },
  {
    id: 'e',
    created: '01/12/2018 14:45',
    start: '11/12/2018 12:00',
    end: '11/12/2018 15:00',
    time: '3',
    machine: {
      displayName: 'DLP-1'
    }
  },
  {
    id: 'f',
    created: '25/11/2018 17:34',
    start: '27/12/2018 20:00',
    end: '28/12/2018 07:00',
    time: '11',
    machine: {
      displayName: 'SLA-1'
    }
  }
];

const supervisorOptions = [
  {
    value: 'a',
    label: 'Andrei-Alexandru Popa'
  },
  {
    value: 'b',
    label: 'Simon Iversen'
  }
];

const studentOptions = [
  {
    value: 'a',
    label: 'Kasper Laursen'
  },
  {
    value: 'b',
    label: 'Frederik Yde Jespersen'
  }
];

const machineTypes = [
  {
    value: 'a',
    label: 'DLP 3D printer'
  },
  {
    value: 'b',
    label: 'CNC devices'
  },
  {
    value: 'c',
    label: 'SLA 3D printer'
  },
  {
    value: 'd',
    label: 'FDM 3D printer'
  }
];

const machines = [
  {
    value: 'a',
    label: 'FDM-1'
  },
  {
    value: 'b',
    label: 'FDM-2'
  },
  {
    value: 'c',
    label: 'FDM-3'
  },
  {
    value: 'd',
    label: 'FDM-4'
  }
];

class Bookings extends Component {
  constructor() {
    super();
    this.state = {
      expanded: {
        pending: '',
        history: '',
        approved: ''
      }
    };
    this.toggleExpansion = this.toggleExpansion.bind(this);
  }

  toggleExpansion(panel, jobId) {
    return () => {
      this.setState(state => ({
        expanded: {
          ...state.expanded,
          [panel]: state.expanded[panel] === jobId ? '' : jobId
        }
      }));
    };
  }

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        <Card>
          <CardHeader
            title="New booking"
            subheader="Request access to a machine"
          />
          <CardContent>
            <Grid spacing={4} container>
              <Grid xs={12} lg={6} item>
                <Select
                  label="Machine type"
                  variant="outlined"
                  options={machineTypes}
                  value={machineTypes[machineTypes.length - 1].value}
                  fullWidth
                  required
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <Select
                  label="Machine"
                  variant="outlined"
                  options={machines}
                  value={machines[0].value}
                  fullWidth
                  required
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <TextField
                  id="desired time"
                  variant="outlined"
                  label="Desired time"
                  defaultValue={4}
                  fullWidth
                  required
                  InputProps={{
                    type: 'number',
                    endAdornment: (
                      <InputAdornment position="start">hours</InputAdornment>
                    )
                  }}
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <Select
                  options={supervisorOptions}
                  value={supervisorOptions[supervisorOptions.length - 1].value}
                  variant="outlined"
                  label="Supervisor"
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} item>
                <Select
                  options={studentOptions}
                  value={studentOptions[studentOptions.length - 1].value}
                  variant="outlined"
                  label="Share with users"
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} item>
                <Grid spacing={4} container direction="row">
                  <Grid item>
                    <FileInputButton variant="contained" color="secondary">
                      Upload gcode
                    </FileInputButton>
                  </Grid>
                  <Grid item>
                    <FileInputButton variant="contained" color="secondary">
                      Upload stl
                    </FileInputButton>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Button variant="contained" color="primary">
                  Submit
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>

        <Card className={classes.card}>
          <CardHeader
            title="Pending"
            subheader="Bookings waiting for approval"
          />
          <CardContent>
            {mockJobs.slice(0, 2).map(job => (
              <Booking
                key={job.id}
                job={job}
                expanded={expanded.pending === job.id}
                onClick={this.toggleExpansion('pending', job.id)}
              />
            ))}
          </CardContent>
        </Card>

        <Card className={classes.card}>
          <CardHeader
            title="Approved"
            subheader="Bookings granting access to machines"
          />
          <CardContent>
            {mockJobs.slice(2, 4).map(job => (
              <Booking
                key={job.id}
                job={job}
                expanded={expanded.approved === job.id}
                onClick={this.toggleExpansion('approved', job.id)}
              />
            ))}
          </CardContent>
        </Card>

        <Card className={classes.card}>
          <CardHeader title="History" subheader="Expired booking approvals" />
          <CardContent>
            {mockJobs.slice(4, 6).map(job => (
              <Booking
                key={job.id}
                job={job}
                expanded={expanded.history === job.id}
                onClick={this.toggleExpansion('history', job.id)}
                expired
              />
            ))}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Bookings.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired
  }).isRequired
};

export default withStyles(styles)(Bookings);
