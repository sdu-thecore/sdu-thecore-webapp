import React from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Grid,
  Button,
  ListItemText,
  withStyles,
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import styles from './styles';

const Booking = ({ expanded, job, classes, expired, onClick, onCancel }) => (
  <Accordion expanded={expanded}>
    <AccordionSummary expandIcon={<ExpandMore />} onClick={onClick}>
      <Grid container spacing={2} alignItems="center">
        <Grid item sm={12} md={4}>
          <ListItemText primary="Machine" secondary={job.machine.displayName} />
        </Grid>
        <Grid item sm={12} md={4}>
          <ListItemText primary="Created" secondary={job.created} />
        </Grid>
      </Grid>
    </AccordionSummary>
    <AccordionDetails>
      <Grid container spacing={2} className={classes.grid} alignItems="center">
        <Grid item sm={12} md={4}>
          <ListItemText primary="Booked from" secondary={job.start} />
        </Grid>
        <Grid item sm={12} md={4}>
          <ListItemText primary="Booked until" secondary={job.end} />
        </Grid>
        <Grid item sm={12} md={4}>
          <ListItemText primary="Booked time" secondary={`${job.time} h`} />
        </Grid>
      </Grid>
    </AccordionDetails>
    {!expired && <Divider />}
    {!expired && (
      <AccordionActions>
        <Button onClick={onCancel} variant="contained" color="primary">
          Cancel booking
        </Button>
      </AccordionActions>
    )}
  </Accordion>
);

Booking.propTypes = {
  expanded: PropTypes.bool,
  expired: PropTypes.bool,
  onCancel: PropTypes.func,
  onClick: PropTypes.func,
  job: PropTypes.shape({
    machine: PropTypes.shape({
      displayName: PropTypes.string.isRequired,
    }).isRequired,
    created: PropTypes.string.isRequired,
    start: PropTypes.string.isRequired,
    end: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
  }).isRequired,
  classes: PropTypes.shape({
    grid: PropTypes.string.isRequired,
  }).isRequired,
};

Booking.defaultProps = {
  onCancel: () => null,
  onClick: () => null,
  expired: false,
  expanded: false,
};

export default withStyles(styles)(Booking);
