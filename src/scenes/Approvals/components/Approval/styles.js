const styles = theme => ({
  grid: {
    paddingRight: theme.spacing(4)
  }
});

export default styles;
