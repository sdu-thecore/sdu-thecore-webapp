import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, CardHeader, withStyles } from '@material-ui/core';
import { FixedLayout } from '../../components';
import Approval from './components/Approval';
import styles from './styles';

const mockPending = [
  {
    id: 'a',
    userName: 'Nicklas Michel Frahm',
    machine: 'FDM-5',
    description:
      'We are currently building a new solution for the workshop administration and are creating IoT devices. These need cases, that we designed. Please find the files attached.',
    hours: 3,
    state: 'pending',
    comment: ''
  },
  {
    id: 'b',
    userName: 'Frederik Yde Jespersen',
    machine: 'SLA-2',
    description:
      'We are currently building a new solution for the workshop administration and are creating IoT devices. These need cases, that we designed. Please find the files attached.',
    hours: 2,
    state: 'pending',
    comment: ''
  }
];

const mockHistory = [
  {
    id: 'a',
    userName: 'Kasper Schmidt Laursen',
    machine: 'CNC-1',
    description:
      'Because the nut for our industrial cable tension reliefs went missing and we are short on time, we need to reprint it.',
    hours: 5,
    state: 'approved',
    comment: 'Approved.'
  },
  {
    id: 'b',
    userName: 'Rasmus Kjær Henriksen',
    machine: 'DLP-2',
    description:
      'Because the nut for our industrial cable tension reliefs went missing and we are short on time, we need to reprint it.',
    hours: 9,
    state: 'denied',
    comment: 'Too much overhang. The chance that the print fails is high.'
  }
];

class Approvals extends Component {
  constructor() {
    super();
    this.state = {
      expanded: {
        pending: '',
        history: ''
      }
    };
    this.toggleExpansion = this.toggleExpansion.bind(this);
  }

  toggleExpansion(panel, approvalId) {
    return () => {
      this.setState(state => ({
        expanded: {
          ...state.expanded,
          [panel]: state.expanded[panel] === approvalId ? '' : approvalId
        }
      }));
    };
  }

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        <Card className={classes.card}>
          <CardHeader title="Pending" subheader="Unapproved requests" />
          <CardContent>
            {mockPending.map(approval => (
              <Approval
                key={approval.id}
                approval={approval}
                expanded={expanded.pending === approval.id}
                onClick={this.toggleExpansion('pending', approval.id)}
              />
            ))}
          </CardContent>
        </Card>

        <Card className={classes.card}>
          <CardHeader title="History" subheader="Expired approvals" />
          <CardContent>
            {mockHistory.map(approval => (
              <Approval
                history
                key={approval.id}
                approval={approval}
                expanded={expanded.history === approval.id}
                onClick={this.toggleExpansion('history', approval.id)}
              />
            ))}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Approvals.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired
  }).isRequired
};

export default withStyles(styles)(Approvals);
