import React from 'react';
import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Grid,
  ListItemText,
  Typography,
  withStyles,
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import { startCase } from 'lodash';
import { connect } from 'react-redux';
import { Select } from '../../components';
import { updateUser } from '../../services/user/actions';
import UserPermissions from './UserPermissions';

const styles = (theme) => ({
  grid: {
    paddingRight: theme.spacing(4),
  },
  listItemText: {
    marginLeft: theme.spacing(1.5),
  },
  typography: {
    marginLeft: theme.spacing(1.5),
  },
});

const roles = ['user', 'staff', 'supervisor', 'admin'].map((role) => ({
  value: role,
  label: startCase(role),
}));

const User = ({
  id,
  user,
  expanded,
  profile,
  onClick,
  dispatchUpdateUserRole,
  classes,
}) => (
  <Accordion expanded={expanded} onChange={onClick(id)}>
    <AccordionSummary expandIcon={<ExpandMore />}>
      <Grid spacing={4} container>
        <Grid xs={12} md={6} item>
          <Typography className={classes.typography} variant="subtitle2">
            {user.displayName}
          </Typography>
        </Grid>
        <Grid xs={12} md={6} item>
          <Typography className={classes.typography} variant="subtitle2">
            {user.email}
          </Typography>
        </Grid>
      </Grid>
    </AccordionSummary>
    <AccordionDetails>
      <Grid spacing={4} container>
        <Grid xs={12} md={6} item>
          <Grid spacing={2} direction="column" container>
            <Grid xs={12} item>
              {profile.role === 'admin' ? (
                <Select
                  label="Role"
                  name="role"
                  variant="outlined"
                  onChange={dispatchUpdateUserRole(user.id)}
                  options={roles}
                  value={user.role}
                  required
                  fullWidth
                />
              ) : (
                <ListItemText
                  className={classes.listItemText}
                  primary="Role"
                  secondary={startCase(user.role)}
                />
              )}
            </Grid>
            <Grid xs={12} item>
              <ListItemText
                className={classes.listItemText}
                primary="Card number"
                secondary={
                  user.cardNumber
                    ? user.cardNumber.substring(user.cardNumber.length - 6)
                    : 'unknown'
                }
              />
            </Grid>
            <Grid xs={12} item>
              <ListItemText
                className={classes.listItemText}
                primary="Card ID"
                secondary={user.cardId ? user.cardId : 'unknown'}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid xs={12} md={6} item>
          <UserPermissions userId={id} />
        </Grid>
      </Grid>
    </AccordionDetails>
  </Accordion>
);

User.propTypes = {
  id: PropTypes.string.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    permissions: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    cardNumber: PropTypes.string,
    cardId: PropTypes.string,
  }).isRequired,
  expanded: PropTypes.bool,
  onClick: PropTypes.func,
  profile: PropTypes.shape({
    role: PropTypes.string.isRequired,
  }).isRequired,
  dispatchUpdateUserRole: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    grid: PropTypes.string.isRequired,
    listItemText: PropTypes.string.isRequired,
    typography: PropTypes.string.isRequired,
  }).isRequired,
};

User.defaultProps = {
  expanded: false,
  onClick: () => null,
};

const mapStateToProps = ({ user }, { id }) => ({
  user: user.users[id],
  profile: user.profile,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateUserRole: (userId) => ({ target }) =>
    dispatch(updateUser(userId, { [target.name]: target.value })),
});

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(User)
);
