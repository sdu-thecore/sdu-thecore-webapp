import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Grid,
  withStyles
} from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroller';
import clsx from 'clsx';
import { FixedLayout } from '../../components';
import { readPermissions } from '../../services/permission/actions';
import { readUsers } from '../../services/user/actions';
import { byProperty, objectToArray } from '../../util';
import { POLL_INTERVAL } from '../../constants';
import { AuthZ } from '../../hocs';
import User from './User';

const styles = theme => ({
  loader: {
    marginTop: theme.spacing(2)
  }
});

class Users extends Component {
  constructor() {
    super();
    this.state = {
      expanded: ''
    };
    this.timer = null;
    this.onChange = this.onChange.bind(this);
    this.onLoadMore = this.onLoadMore.bind(this);
  }

  componentDidMount() {
    const {
      dispatchReadUsers,
      dispatchReadPermissions,
      userIds,
      permissionIds
    } = this.props;
    if (!userIds.length) {
      dispatchReadUsers('display_name');
    }
    if (!permissionIds.length) {
      dispatchReadPermissions('created');
    }
    if (!this.timer) {
      this.timer = setInterval(() => {
        dispatchReadUsers('display_name');
        dispatchReadPermissions('created');
      }, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  onChange(panel) {
    return () => {
      this.setState(state => ({
        expanded: state.expanded === panel ? '' : panel
      }));
    };
  }

  onLoadMore() {
    const { dispatchReadUsers } = this.props;
    dispatchReadUsers('display_name');
  }

  render() {
    const { userIds, loading, hasNextPage, classes } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        <Card>
          <CardHeader
            title='Users'
            subheader='Registered users tracked by the system'
          />
          <CardContent>
            {userIds.length ? (
              <InfiniteScroll
                loadMore={this.onLoadMore}
                hasMore={!loading && hasNextPage}
                useWindow={false}
              >
                {userIds.map(userId => (
                  <User
                    key={userId}
                    id={userId}
                    expanded={expanded === userId}
                    onClick={this.onChange}
                  />
                ))}
              </InfiniteScroll>
            ) : null}
            {loading && !userIds.length ? (
              <Grid
                className={clsx({ [classes.loader]: userIds.length > 0 })}
                spacing={2}
                justify='center'
                container
              >
                <Grid item>
                  <CircularProgress color='secondary' />
                </Grid>
              </Grid>
            ) : null}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Users.propTypes = {
  userIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  permissionIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  loading: PropTypes.bool.isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  dispatchReadUsers: PropTypes.func.isRequired,
  dispatchReadPermissions: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    loader: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ user, permission }) => ({
  hasNextPage: user.hasNextPage,
  loading: user.loading,
  userIds: objectToArray(user.users)
    .sort(byProperty('displayName'))
    .map(u => u.id),
  permissionIds: objectToArray(permission.permissions)
    .sort(byProperty('abbreviation'))
    .map(p => p.id)
});

const mapDispatchToProps = dispatch => ({
  dispatchReadUsers: sortBy => dispatch(readUsers(sortBy)),
  dispatchReadPermissions: sortBy => dispatch(readPermissions(sortBy))
});

export default AuthZ({
  roles: ['staff', 'admin'],
  redirect: true,
  redirectTarget: '/dashboard'
})(withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Users)));
