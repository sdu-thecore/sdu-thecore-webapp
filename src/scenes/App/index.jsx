import React from 'react';
import { CssBaseline, MuiThemeProvider } from '@material-ui/core';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import AppLayout from '../../components/AppLayout/AppLayout';
import store from '../../store';
import theme from '../../theme';
import Profile from '../Profile/Profile';
import Help from '../Help';
import Users from '../Users/Users';
import Dashboard from '../Dashboard/Dashboard';
import Bookings from '../Bookings';
import Nodes from '../Nodes/Nodes';
import ReadPermissions from '../Permissions/ReadPermissions';
import ReadMachines from '../Machines/ReadMachines';
import Approvals from '../Approvals';
import Report from '../Report';
import ReadToolTypes from '../ToolTypes/ReadToolTypes';
import ReadTools from '../Tools/ReadTools';
import MachineCameraStream from '../Machines/MachineCameraStream';

const App = () => (
  <>
    <CssBaseline />
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <AppLayout>
            <Switch>
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/bookings" component={Bookings} />
              <Route
                path="/machines/:nodeId/livestream"
                component={MachineCameraStream}
              />
              <Route path="/machines" component={ReadMachines} />
              <Route
                path="/tool_types/:toolTypeId"
                exact
                component={ReadTools}
              />
              <Route path="/tool_types" exact component={ReadToolTypes} />
              <Route path="/report" component={Report} />
              <Route path="/feedback" component={Help} />
              <Route path="/profile" component={Profile} />
              <Route path="/approvals" component={Approvals} />
              <Route path="/users" component={Users} />
              <Route path="/nodes" component={Nodes} />
              <Route path="/help" component={Help} />
              <Route path="/permissions" component={ReadPermissions} />
              <Redirect to="/dashboard" />
            </Switch>
          </AppLayout>
        </BrowserRouter>
      </MuiThemeProvider>
    </Provider>
  </>
);

export default App;
