const styles = theme => ({
  loader: {
    marginTop: theme.spacing(2)
  }
});

export default styles;
