import React from 'react';
import Chip from '@material-ui/core/Chip';
import { VpnKeyRounded } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  chip: {
    backgroundColor: theme.palette.background.paper,
  },
  key: {
    color: theme.palette.primary.main,
  },
}));

const NewSessionStatus = ({ sessions }) => {
  const classes = useStyles();

  const authorizedSession = sessions.find((session) => session.authorized);
  const newSessions = sessions.filter((session) => {
    if (session.authorized) return false;
    if (!authorizedSession) return !session.authorized;
    return (
      !session.authorized &&
      session.created.localeCompare(authorizedSession.created) > 0
    );
  }).length;

  const str = newSessions > 1 ? 'sessions' : 'session';

  return newSessions ? (
    <Chip
      className={classes.chip}
      icon={<VpnKeyRounded className={classes.key}></VpnKeyRounded>}
      label={`${newSessions} new ${str}`}
    />
  ) : null;
};

export default NewSessionStatus;
