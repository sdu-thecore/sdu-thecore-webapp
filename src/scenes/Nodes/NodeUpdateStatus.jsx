import React, { useState, useEffect } from 'react';
import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';
import { CheckCircle, Error as ErrorIcon } from '@material-ui/icons';
import { ENDPOINT_APPLICATION } from '../../endpoints';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  success: {
    color: theme.palette.success.main,
  },
  warning: {
    color: theme.palette.warning.main,
  },
  chip: {
    backgroundColor: theme.palette.background.paper,
  },
}));

const NodeUpdateStatus = ({ node }) => {
  const classes = useStyles();

  const [version, setVersion] = useState(node.applicationVersion || '');

  useEffect(() => {
    let mounted = true;

    (async () => {
      try {
        if (node.applicationName) {
          const res = await axios(
            ENDPOINT_APPLICATION(node.applicationName)({
              release_channel: node.releaseChannel,
              cpu_architecture: 'arm',
            })
          );

          if (mounted) {
            setVersion(res?.data?.data?.commit_sha);
          }
        }
      } catch (err) {
        // Ignore error, the default value will be used.
      }
    })();

    return () => {
      mounted = false;
    };
  }, [node.applicationName, node.releaseChannel]);

  const icon =
    node.applicationVersion === version ? (
      <CheckCircle className={classes.success}></CheckCircle>
    ) : (
      <ErrorIcon className={classes.warning}></ErrorIcon>
    );

  return (
    <span>
      <Chip className={classes.chip} icon={icon} label={node.releaseChannel} />
    </span>
  );
};

export default NodeUpdateStatus;
