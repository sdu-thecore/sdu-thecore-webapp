import React from 'react';
import PropTypes from 'prop-types';
import { TextField, MenuItem } from '@material-ui/core';
import InfiniteScroller from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { readToolTypes } from '../../services/toolType/actions';
import { objectToArray, byProperty } from '../../util';

const ToolTypeSelect = ({
  isLoading,
  hasNextPage,
  toolTypes,
  dispatchReadEntities,
  ...props
}) => {
  const loadMore = () => dispatchReadEntities('name');
  /* eslint-disable react/jsx-props-no-spreading */
  return (
    <TextField
      component={InfiniteScroller}
      loadMore={loadMore}
      hasMore={!isLoading && hasNextPage}
      {...props}
      required
      select
    >
      {toolTypes.length ? (
        toolTypes.map(toolType => (
          <MenuItem key={toolType.id} value={toolType.id}>
            {toolType.name}
          </MenuItem>
        ))
      ) : (
        <MenuItem key="loading" value="loading">
          Loading options ...
        </MenuItem>
      )}
    </TextField>
  );
  /* eslint-enable react/jsx-props-no-spreading */
};

ToolTypeSelect.propTypes = {
  dispatchReadEntities: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  toolTypes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

const mapStateToProps = ({ toolType }) => ({
  toolTypes: objectToArray(toolType.entities).sort(byProperty('name')),
  isLoading: toolType.isReadingMany,
  hasNextPage: toolType.hasNextPage
});

const mapDispatchToProps = dispatch => ({
  dispatchReadEntities: sortOrder => dispatch(readToolTypes(sortOrder))
});

export default connect(mapStateToProps, mapDispatchToProps)(ToolTypeSelect);
