import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Fab,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  withStyles,
} from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { compose } from 'recompose';
import { byProperty, objectToArray } from '../../util';
import { AuthZ } from '../../hocs';
import { useInitialAndPeriodicAction } from '../../hooks';
import { readTools } from '../../services/tool/actions';
import UpdateTool from './UpdateTool';

const styles = (theme) => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing(4),
    // note that the 16 pixels compensate for the scrollbar
    right: 16 + theme.spacing(4),
  },
  dialogContent: {
    paddingTop: theme.spacing(2),
  },
});

const UnassignedTools = ({
  classes,
  tools,
  dispatchReadEntities,
  hasNextPage,
  loading,
}) => {
  const [state, setState] = useState({
    isDialogOpen: false,
    isUpdateToolOpen: false,
  });
  const openDialog = () =>
    setState((prevState) => ({ ...prevState, isDialogOpen: true }));
  const closeDialog = () =>
    setState((prevState) => ({ ...prevState, isDialogOpen: false }));
  const openUpdateTool = () =>
    setState((prevState) => ({ ...prevState, isUpdateToolOpen: true }));
  const closeUpdateTool = () =>
    setState((prevState) => ({ ...prevState, isUpdateToolOpen: false }));
  useInitialAndPeriodicAction(() => {
    dispatchReadEntities('created');
  }, 1000);
  const loadMore = () => dispatchReadEntities('created');
  return (
    <>
      <Fab
        color="primary"
        aria-label="Create new tool type"
        className={classes.fab}
        onClick={openDialog}
      >
        <Add />
      </Fab>
      <Dialog
        fullWidth
        maxWidth="md"
        open={state.isDialogOpen}
        onClose={closeDialog}
        scroll="body"
        aria-labelledby="UnassignedTools_Title"
      >
        <DialogTitle id="UnassignedTools_Title">
          Select an unassigned tool
        </DialogTitle>
        <DialogContent className={classes.dialogContent}>
          <Paper>
            <Table padding="default">
              <TableHead>
                <TableRow>
                  <TableCell>Database ID</TableCell>
                  <TableCell>RFID tag ID</TableCell>
                  <TableCell>Created</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              {tools.length ? (
                <TableBody
                  component={InfiniteScroll}
                  element="tbody"
                  loadMore={loadMore}
                  hasMore={!loading && hasNextPage}
                  useWindow={false}
                >
                  {tools.map((tool) => (
                    <TableRow key={tool.id}>
                      <TableCell>{tool.id}</TableCell>
                      <TableCell>{tool.tagId}</TableCell>
                      <TableCell>{tool.created}</TableCell>
                      <TableCell>
                        <IconButton onClick={openUpdateTool}>
                          <Add />
                        </IconButton>
                        <UpdateTool
                          open={state.isUpdateToolOpen}
                          onClose={closeUpdateTool}
                          id={tool.id}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              ) : (
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={4}>
                      <b>There are currently no unassigned tools.</b>
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </Paper>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} autoFocus>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

UnassignedTools.propTypes = {
  dispatchReadEntities: PropTypes.func.isRequired,
  tools: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      tagId: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  classes: PropTypes.shape({
    fab: PropTypes.string.isRequired,
    dialogContent: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ tool }) => ({
  tools: objectToArray(tool.entities)
    .filter((item) => !item.type)
    .sort(byProperty('created')),
  hasNextPage: tool.hasNextPage,
  loading: tool.isReadingMany,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchReadEntities: (sortOrder) => dispatch(readTools(sortOrder)),
});

export default compose(
  AuthZ({ roles: ['staff', 'admin'] }),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(UnassignedTools);
