import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  LinearProgress,
  withStyles
} from '@material-ui/core';
import { get } from 'lodash';
import { FixedLayout } from '../../components';
import { classname } from '../../util';
import useToolTypeOrNavigate from './hooks';
import ToolTable from './ToolTable';
import UnassignedTools from './UnassignedTools';

const styles = theme => ({
  cardHeaderLoading: {
    paddingTop: theme.spacing(2) - 4
  }
});

const ReadTools = ({ match, history, classes }) => {
  const [state] = useToolTypeOrNavigate(match.params.toolTypeId, history);
  return (
    <FixedLayout>
      <Card>
        {state.loading && <LinearProgress color="primary" />}
        <CardHeader
          className={classname(state.loading, classes.cardHeaderLoading)}
          title={get(state, 'toolType.name', 'Unknown tool type')}
          subheader={get(state, 'toolType.description', '')}
        />
        <CardContent>
          <ToolTable toolTypeId={match.params.toolTypeId || ''} />
        </CardContent>
      </Card>
      <UnassignedTools />
    </FixedLayout>
  );
};

ReadTools.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      toolTypeId: PropTypes.string.isRequired
    }).isRequired
  }).isRequired,
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired
  }).isRequired,
  classes: PropTypes.shape({
    cardHeaderLoading: PropTypes.string.isRequired
  }).isRequired
};

export default withStyles(styles)(ReadTools);
