import React from 'react';
import PropTypes from 'prop-types';
import { TableCell, TableRow, withStyles } from '@material-ui/core';
import { Check, Close, NotInterested } from '@material-ui/icons';
import { connect } from 'react-redux';
import { ENDPOINT_IMAGE_CONTENT } from '../../endpoints';
import DeletePermission from './DeletePermission';
import UpdatePermission from './UpdatePermission';
import { schedulingOptions, workflowOptions, byValue } from './common';

const styles = theme => ({
  thumbnail: {
    width: theme.spacing(8)
  }
});

const TableRowPermission = ({ permission, classes }) => (
  <TableRow>
    <TableCell>
      {permission.image ? (
        <img
          className={classes.thumbnail}
          src={ENDPOINT_IMAGE_CONTENT(permission.image)}
          alt={`${permission.name}`}
        />
      ) : (
        <NotInterested className={classes.thumbnail} fontSize="large" />
      )}
    </TableCell>
    <TableCell>{permission.abbreviation}</TableCell>
    <TableCell>{permission.name}</TableCell>
    <TableCell>{permission.description || <i>None</i>}</TableCell>
    <TableCell>
      {schedulingOptions.find(byValue(permission.scheduling)).label}
    </TableCell>
    <TableCell>
      {workflowOptions.find(byValue(permission.workflow)).label}
    </TableCell>
    <TableCell>{permission.default ? <Check /> : <Close />}</TableCell>
    <TableCell>
      <UpdatePermission id={permission.id} />
      <DeletePermission id={permission.id} />
    </TableCell>
  </TableRow>
);

TableRowPermission.propTypes = {
  permission: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    scheduling: PropTypes.string.isRequired,
    workflow: PropTypes.string.isRequired,
    default: PropTypes.bool.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.shape({
    thumbnail: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ permission }, { id }) => ({
  permission: permission.permissions[id]
});

export default withStyles(styles)(connect(mapStateToProps)(TableRowPermission));
