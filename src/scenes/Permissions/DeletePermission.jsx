import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton
} from '@material-ui/core';
import { DeleteOutlined } from '@material-ui/icons';
import { connect } from 'react-redux';
import { deletePermission } from '../../services/permission/actions';
import { deleteImage } from '../../imageRequests';

class DeletePermission extends Component {
  constructor() {
    super();
    this.state = {
      isDialogOpen: false
    };
    this.getPermissionNameMarkup = this.getPermissionNameMarkup.bind(this);
    this.deletePermission = this.deletePermission.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.openDialog = this.openDialog.bind(this);
  }

  getPermissionNameMarkup() {
    const { permission } = this.props;
    if (!permission.name || !permission.abbreviation) {
      return '';
    }
    return (
      <b>
        {permission.name} ({permission.abbreviation})
      </b>
    );
  }

  async deletePermission() {
    const { dispatchDeletePermission, permission, bearerToken } = this.props;
    if (permission.image) {
      try {
        await deleteImage(bearerToken, permission.image);
      } catch (err) {
        // ignore if image does not exists
      }
    }
    dispatchDeletePermission(permission.id);
    this.closeDialog();
  }

  openDialog() {
    this.setState(state => ({ ...state, isDialogOpen: true }));
  }

  closeDialog() {
    this.setState(state => ({ ...state, isDialogOpen: false }));
  }

  render() {
    const { permission } = this.props;
    const { isDialogOpen } = this.state;
    return (
      <>
        <IconButton onClick={this.openDialog}>
          <DeleteOutlined />
        </IconButton>
        <Dialog
          open={isDialogOpen}
          onClose={this.closeDialog}
          aria-labelledby={`DeletePermission_Title_${permission.id}`}
          aria-describedby={`DeletePermission_Description_${permission.id}`}
        >
          <DialogTitle id={`DeletePermission_Title_${permission.id}`}>
            Delete this permission?
          </DialogTitle>
          <DialogContent>
            <DialogContentText
              id={`DeletePermission_Description_${permission.id}`}
              align="justify"
            >
              The {this.getPermissionNameMarkup()} permission will be
              permanently deleted. Deleting a permission cannot be undone and
              will remove all existing references. Machines and users might
              require configuration in order to be accessed afterwards.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} autoFocus>
              Cancel
            </Button>
            <Button onClick={this.deletePermission} color="primary">
              Delete permission
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

DeletePermission.propTypes = {
  permission: PropTypes.shape({
    id: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  bearerToken: PropTypes.string.isRequired,
  dispatchDeletePermission: PropTypes.func.isRequired
};

const mapStateToProps = ({ permission, auth }, { id }) => ({
  bearerToken: auth.token,
  permission: permission.permissions[id]
});

const mapDispatchToProps = dispatch => ({
  dispatchDeletePermission: id => dispatch(deletePermission(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DeletePermission);
