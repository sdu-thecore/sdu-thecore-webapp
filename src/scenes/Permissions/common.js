export const schedulingOptions = [
  // { value: 'scheduled', label: 'Scheduled' },
  {
    value: 'on_demand',
    label: 'On demand',
    description:
      'Devices are assigned to a user as long as the card of the user is detected. As soon as the card is removed, the machine is released and available for the next user.',
  },
  {
    value: 'locking',
    label: 'Locking',
    description:
      'Scanning the card the first time will open the door and assign the machine to the user. Scanning the card a second time will open the door again and release the machine.',
  },
];

export const workflowOptions = [
  // { value: 'controlled', label: 'Controlled' },
  // { value: 'timed', label: 'Timed' },
  {
    value: 'open',
    label: 'Open',
    description:
      'Users can directly access the machine without prior approval.',
  },
];

export const byValue = (value) => (item) => item.value === value;
