import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  withStyles,
  Typography,
} from '@material-ui/core';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { FixedLayout } from '../../components';
import { readPermissions } from '../../services/permission/actions';
import { POLL_INTERVAL } from '../../constants';
import { objectToArray, byProperty } from '../../util';
import CreatePermission from './CreatePermission';
import TableRowPermission from './TableRowPermission';
import ExpansionCardPermission from './ExpansionCardPermission';

const styles = (theme) => ({
  card: {
    marginTop: theme.spacing(2),
  },
});

class ReadPermissions extends Component {
  constructor() {
    super();
    this.state = {
      expanded: '',
    };
    this.timer = null;
    this.toggleExpansion = this.toggleExpansion.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    const { dispatchReadPermissions, permissionIds } = this.props;
    if (!permissionIds.length) {
      dispatchReadPermissions('created');
    }
    if (!this.timer) {
      this.timer = setInterval(() => {
        dispatchReadPermissions('created');
      }, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  toggleExpansion(permissionId) {
    return () => {
      this.setState((state) => ({
        expanded: state.expanded === permissionId ? '' : permissionId,
      }));
    };
  }

  loadMore() {
    const { dispatchReadPermissions } = this.props;
    dispatchReadPermissions('created');
  }

  render() {
    const { permissionIds, classes, loading, hasNextPage } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        <CreatePermission />
        <Card className={classes.card}>
          <CardHeader title="Permissions" subheader="Available device groups" />
          <CardContent>
            <Hidden lgUp>
              {permissionIds.length ? (
                <InfiniteScroll
                  loadMore={this.loadMore}
                  hasMore={!loading && hasNextPage}
                  useWindow={false}
                >
                  {permissionIds.map((permissionId) => (
                    <ExpansionCardPermission
                      key={permissionId}
                      id={permissionId}
                      expanded={expanded === permissionId}
                      onClick={this.toggleExpansion(permissionId)}
                    />
                  ))}
                </InfiniteScroll>
              ) : (
                <Typography>
                  <b>There are currently no permissions.</b>
                </Typography>
              )}
            </Hidden>
            <Hidden mdDown>
              <Paper>
                <Table padding="default">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">Image</TableCell>
                      <TableCell>Abbreviation</TableCell>
                      <TableCell>Name</TableCell>
                      <TableCell>Description</TableCell>
                      <TableCell>Scheduling</TableCell>
                      <TableCell>Workflow</TableCell>
                      <TableCell>Default permission</TableCell>
                      <TableCell>Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  {permissionIds.length ? (
                    <TableBody
                      component={InfiniteScroll}
                      element="tbody"
                      loadMore={this.loadMore}
                      hasMore={!loading && hasNextPage}
                      useWindow={false}
                    >
                      {permissionIds.map((permissionId) => (
                        <TableRowPermission
                          key={permissionId}
                          id={permissionId}
                        />
                      ))}
                    </TableBody>
                  ) : (
                    <TableBody>
                      <TableRow>
                        <TableCell colSpan={5}>
                          <b>There are currently no permissions.</b>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )}
                </Table>
              </Paper>
            </Hidden>
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

ReadPermissions.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired,
  }).isRequired,
  permissionIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  loading: PropTypes.bool.isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  dispatchReadPermissions: PropTypes.func.isRequired,
};

const mapStateToProps = ({ permission }) => ({
  hasNextPage: permission.hasNextPage,
  loading: permission.loading,
  permissionIds: objectToArray(permission.permissions)
    .sort(byProperty('created'))
    .map((p) => p.id),
});

const mapDispatchToProps = (dispatch) => ({
  dispatchReadPermissions: (sortBy) => dispatch(readPermissions(sortBy)),
});

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(ReadPermissions)
);
