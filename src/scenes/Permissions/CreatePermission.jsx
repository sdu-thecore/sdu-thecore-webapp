import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Grid,
  LinearProgress,
  TextField,
  Typography,
  withStyles
} from '@material-ui/core';
import { withFormik } from 'formik';
import { isFunction } from 'lodash';
import { object, string, bool } from 'yup';
import clsx from 'clsx';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { Select, Switch, ImageUpload } from '../../components';
import { createPermission } from '../../services/permission/actions';
import { createImage } from '../../imageRequests';
import { schedulingOptions, workflowOptions, byValue } from './common';

const styles = theme => ({
  cardHeaderLoading: {
    paddingTop: theme.spacing(2) - 4
  }
});

class CreatePermission extends Component {
  constructor() {
    super();
    this.defaultState = {
      imageUploadState: {
        isDialogOpen: false,
        isBusyCropping: false,
        zoom: 1,
        crop: { x: 0, y: 0 },
        croppedArea: { x: 0, y: 0, width: 0, height: 0 },
        croppedAreaPixels: { x: 0, y: 0, width: 0, height: 0 },
        blob: null,
        filename: '',
        croppedDataUrl: '',
        originalDataUrl: ''
      }
    };
    this.state = this.defaultState;
    this.setImageUploadState = this.setImageUploadState.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { isCreatingOne } = this.props;
    if (prevProps.isCreatingOne && !isCreatingOne) {
      this.resetForm();
    }
  }

  setImageUploadState(nextState, callback) {
    this.setState(
      state => ({
        ...state,
        imageUploadState: {
          ...state.imageUploadState,
          ...(isFunction(nextState)
            ? nextState(state.imageUploadState)
            : nextState)
        }
      }),
      callback
    );
  }

  resetForm() {
    const { imageUploadState } = this.defaultState;
    const { resetForm } = this.props;
    resetForm();
    this.setState({ imageUploadState });
  }

  async submitForm() {
    const {
      bearerToken,
      submitForm,
      setFieldValue,
      setSubmitting
    } = this.props;
    const { imageUploadState } = this.state;
    const { blob, filename } = imageUploadState;
    if (blob) {
      setSubmitting(true);
      const res = await createImage(bearerToken, blob, filename);
      if (res.data && res.data.data && res.data.data.id) {
        setFieldValue('image', res.data.data.id, false);
        submitForm();
      } else {
        setSubmitting(false);
      }
    }
  }

  render() {
    const {
      handleBlur,
      handleChange,
      errors,
      values,
      touched,
      isValid,
      isSubmitting,
      isCreatingOne,
      classes
    } = this.props;
    const { imageUploadState } = this.state;
    const isLoading = isSubmitting || isCreatingOne;
    const schedulingOption = schedulingOptions.find(byValue(values.scheduling));
    const workflowOption = workflowOptions.find(byValue(values.workflow));
    return (
      <Card>
        {isLoading && <LinearProgress color='primary' />}
        <CardHeader
          className={clsx({
            [classes.cardHeaderLoading]: isLoading
          })}
          title='New permission'
          subheader='Create a new device group'
        />
        <CardContent>
          <Grid spacing={4} container>
            <Grid xs={12} lg={6} item>
              <TextField
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.name}
                helperText={touched.name && errors.name}
                error={!!(touched.name && errors.name)}
                name='name'
                label='Name'
                variant='outlined'
                required
                fullWidth
              />
            </Grid>
            <Grid xs={12} lg={6} item>
              <TextField
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.abbreviation}
                helperText={touched.abbreviation && errors.abbreviation}
                error={!!(touched.abbreviation && errors.abbreviation)}
                name='abbreviation'
                label='Abbreviation'
                variant='outlined'
                required
                fullWidth
              />
            </Grid>
            <Grid xs={12} lg={12} item>
              <TextField
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.description}
                helperText={touched.description && errors.description}
                error={!!(touched.description && errors.description)}
                name='description'
                label='Description'
                variant='outlined'
                multiline
                fullWidth
                rows={5}
              />
            </Grid>
            <Grid xs={12} lg={6} item>
              <Select
                options={schedulingOptions}
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.scheduling}
                helperText={touched.scheduling && errors.scheduling}
                error={touched.scheduling && errors.scheduling}
                label='Scheduling'
                name='scheduling'
                variant='outlined'
                required
                fullWidth
              />
            </Grid>
            <Grid xs={12} lg={6} item>
              <Typography align='justify'>
                <b>{schedulingOption.label}:</b> {schedulingOption.description}
              </Typography>
            </Grid>
            <Grid xs={12} lg={6} item>
              <Select
                options={workflowOptions}
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.workflow}
                helperText={touched.workflow && errors.workflow}
                error={touched.workflow && errors.workflow}
                label='Workflow'
                name='workflow'
                variant='outlined'
                required
                fullWidth
              />
            </Grid>
            <Grid xs={12} lg={6} item>
              <Typography align='justify'>
                <b>{workflowOption.label}:</b> {workflowOption.description}
              </Typography>
            </Grid>
            <Grid xs={12} item>
              <Switch
                onBlur={handleBlur}
                onChange={handleChange}
                checked={values.default}
                error={touched.default && errors.default}
                label={
                  <>
                    <b>Default permission: </b>Users will have this permission
                    by default and it is not required to add users explicitly.
                    Revoking access can be done on a per-user basis.
                  </>
                }
                name='default'
                color='primary'
              />
            </Grid>
            <Grid xs={12} item>
              <ImageUpload
                ButtonProps={{ variant: 'contained', color: 'primary' }}
                value={imageUploadState}
                onChange={this.setImageUploadState}
                required
              />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button
            variant='contained'
            color='primary'
            onClick={this.submitForm}
            disabled={!isValid || isLoading || !imageUploadState.blob}
          >
            Create permission
          </Button>
        </CardActions>
      </Card>
    );
  }
}

CreatePermission.propTypes = {
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    scheduling: PropTypes.string.isRequired,
    workflow: PropTypes.string.isRequired,
    default: PropTypes.bool.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  errors: PropTypes.shape({
    name: PropTypes.string,
    abbreviation: PropTypes.string,
    description: PropTypes.string,
    scheduling: PropTypes.string,
    workflow: PropTypes.string,
    default: PropTypes.bool,
    image: PropTypes.string
  }).isRequired,
  touched: PropTypes.shape({
    name: PropTypes.bool,
    abbreviation: PropTypes.bool,
    description: PropTypes.bool,
    scheduling: PropTypes.bool,
    workflow: PropTypes.bool,
    default: PropTypes.bool,
    image: PropTypes.bool
  }).isRequired,
  submitForm: PropTypes.func.isRequired,
  setSubmitting: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  bearerToken: PropTypes.string.isRequired,
  isCreatingOne: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  resetForm: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    cardHeaderLoading: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ permission, auth }) => ({
  bearerToken: auth.token,
  isCreatingOne: permission.isCreatingOne
});

const mapDispatchToProps = dispatch => ({
  dispatchCreatePermission: entity => dispatch(createPermission(entity))
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
  withFormik({
    displayName: 'CreatePermission',
    handleSubmit: (values, { props, setSubmitting }) => {
      const { dispatchCreatePermission } = props;
      dispatchCreatePermission(values);
      setSubmitting(false);
    },
    mapPropsToValues: () => ({
      name: '',
      abbreviation: '',
      description: '',
      scheduling: 'on_demand',
      workflow: 'open',
      default: true,
      image: ''
    }),
    validationSchema: object().shape({
      name: string()
        .strict()
        .trim('The name must not start or end with spaces.')
        .required('The name is required.')
        .min(2, 'The name must be at least two characters long.'),
      abbreviation: string()
        .strict()
        .trim('The abbreviation must not be enclosed in whitespaces.')
        .required('The abbreviation is required.')
        .uppercase('The abbreviation must be uppercase.')
        .length(3, 'The abbreviation must be exactly three characters long.'),
      description: string()
        .strict()
        .trim('The abbreviation must not start or end with spaces.')
        .max(1000, 'The description must not be longer than 1000 characters.'),
      scheduling: string().required('Please select a scheduling option.'),
      workflow: string().required('Please select a workflow option.'),
      default: bool().required(
        'Please select if the permission should automatically be assigned to all users.'
      ),
      image: string().strict()
    })
  })
)(CreatePermission);
