import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  LinearProgress,
  TextField,
  Typography,
  withStyles
} from '@material-ui/core';
import { EditOutlined } from '@material-ui/icons';
import { withFormik } from 'formik';
import { isFunction, get } from 'lodash';
import { object, string, bool } from 'yup';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { ENDPOINT_IMAGE_CONTENT } from '../../endpoints';
import { Select, Switch, ImageUpload } from '../../components';
import { updatePermission } from '../../services/permission/actions';
import { schedulingOptions, workflowOptions, byValue } from './common';
import { createImage, deleteImage } from '../../imageRequests';

const styles = theme => ({
  dialogTitleLoading: {
    paddingTop: theme.spacing(3) - 4
  },
  dialogContent: {
    paddingTop: theme.spacing(2)
  }
});

class UpdatePermission extends Component {
  constructor(props) {
    super(props);
    this.defaultState = {
      isDialogOpen: false,
      imageUploadState: {
        isDialogOpen: false,
        isBusyCropping: false,
        zoom: 1,
        crop: { x: 0, y: 0 },
        croppedArea: { x: 0, y: 0, width: 0, height: 0 },
        croppedAreaPixels: { x: 0, y: 0, width: 0, height: 0 },
        blob: null,
        filename: '',
        croppedDataUrl: '',
        originalDataUrl: ''
      }
    };
    const { permission } = this.props;
    this.state = {
      ...this.defaultState,
      imageUploadState: {
        ...this.defaultState.imageUploadState,
        croppedDataUrl: ENDPOINT_IMAGE_CONTENT(permission.image)
      }
    };
    this.getPermissionNameMarkup = this.getPermissionNameMarkup.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.applyPermission = this.applyPermission.bind(this);
    this.commitPermission = this.commitPermission.bind(this);
    this.setImageUploadState = this.setImageUploadState.bind(this);
    this.isCroppedImageUpdated = this.isCroppedImageUpdated.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { isUpdatingOne } = this.props;
    if (prevProps.isUpdatingOne && !isUpdatingOne) {
      this.resetForm();
    }
    this.ensureImagePreview(prevProps);
  }

  setImageUploadState(nextState, callback) {
    this.setState(
      state => ({
        ...state,
        imageUploadState: {
          ...state.imageUploadState,
          ...(isFunction(nextState)
            ? nextState(state.imageUploadState)
            : nextState)
        }
      }),
      callback
    );
  }

  getPermissionNameMarkup() {
    const { permission } = this.props;
    if (!permission.name || !permission.abbreviation) {
      return '';
    }
    return (
      <b>
        {permission.name} ({permission.abbreviation})
      </b>
    );
  }

  resetForm() {
    const { imageUploadState } = this.defaultState;
    const { resetForm } = this.props;
    resetForm();
    this.setState({ imageUploadState });
  }

  ensureImagePreview(prevProps) {
    const { permission } = this.props;
    const { imageUploadState } = this.state;
    const hasImageChanged =
      prevProps.permission.image &&
      prevProps.permission.image !== permission.image;
    const imagePreviewNeedsUpdate =
      permission.image && (!imageUploadState.croppedDataUrl || hasImageChanged);
    if (imagePreviewNeedsUpdate) {
      this.setState((state, props) => ({
        imageUploadState: {
          ...state.imageUploadState,
          croppedDataUrl: ENDPOINT_IMAGE_CONTENT(props.permission.image)
        }
      }));
    }
  }

  isCroppedImageUpdated() {
    const { imageUploadState } = this.state;
    return /^data:/.test(imageUploadState.croppedDataUrl);
  }

  async applyPermission() {
    const {
      bearerToken,
      submitForm,
      setFieldValue,
      setSubmitting,
      permission
    } = this.props;
    const { imageUploadState } = this.state;
    const { blob, filename } = imageUploadState;
    let id = permission.image;
    setSubmitting(true);
    if (this.isCroppedImageUpdated()) {
      try {
        const res = await createImage(bearerToken, blob, filename);
        id = get(res, 'data.data.id', '');
        if (id) {
          try {
            await deleteImage(bearerToken, permission.image);
          } catch (err) {
            // ignore if image does not exists
          }
        }
      } catch (err) {
        // ignore if image cannot be created and keep existing image
      }
    }
    setFieldValue('image', id, false);
    submitForm();
  }

  async commitPermission() {
    await this.applyPermission();
    this.closeDialog();
  }

  openDialog() {
    this.setState({ isDialogOpen: true });
  }

  closeDialog() {
    this.setState({ isDialogOpen: false });
  }

  render() {
    const {
      handleBlur,
      handleChange,
      errors,
      values,
      touched,
      isValid,
      isSubmitting,
      isUpdatingOne,
      classes,
      permission
    } = this.props;
    const { isDialogOpen, imageUploadState } = this.state;
    const schedulingOption = schedulingOptions.find(byValue(values.scheduling));
    const workflowOption = workflowOptions.find(byValue(values.workflow));
    const isCroppedImageUpdated = this.isCroppedImageUpdated();
    return (
      <>
        <IconButton onClick={this.openDialog}>
          <EditOutlined />
        </IconButton>
        <Dialog
          open={isDialogOpen}
          onClose={this.closeDialog}
          scroll='body'
          aria-labelledby={`UpdatePermission_Title_${permission.id}`}
        >
          {(isSubmitting || isUpdatingOne) && (
            <LinearProgress color='primary' />
          )}
          <DialogTitle
            id={`UpdatePermission_Title_${permission.id}`}
            className={clsx({
              [classes.dialogTitleLoading]: isSubmitting || isUpdatingOne
            })}
          >
            Edit permission
          </DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <Grid spacing={4} container>
              <Grid xs={12} lg={6} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.name}
                  helperText={touched.name && errors.name}
                  error={!!(touched.name && errors.name)}
                  name='name'
                  label='Name'
                  variant='outlined'
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.abbreviation}
                  helperText={touched.abbreviation && errors.abbreviation}
                  error={!!(touched.abbreviation && errors.abbreviation)}
                  name='abbreviation'
                  label='Abbreviation'
                  variant='outlined'
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} lg={12} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.description}
                  helperText={touched.description && errors.description}
                  error={!!(touched.description && errors.description)}
                  name='description'
                  label='Description'
                  variant='outlined'
                  multiline
                  fullWidth
                  rows={5}
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <Select
                  options={schedulingOptions}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.scheduling}
                  helperText={touched.scheduling && errors.scheduling}
                  error={touched.scheduling && errors.scheduling}
                  label='Scheduling'
                  name='scheduling'
                  variant='outlined'
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <Typography align='justify'>
                  <b>{schedulingOption.label}:</b>{' '}
                  {schedulingOption.description}
                </Typography>
              </Grid>
              <Grid xs={12} lg={6} item>
                <Select
                  options={workflowOptions}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.workflow}
                  helperText={touched.workflow && errors.workflow}
                  error={touched.workflow && errors.workflow}
                  label='Workflow'
                  name='workflow'
                  variant='outlined'
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} lg={6} item>
                <Typography align='justify'>
                  <b>{workflowOption.label}:</b> {workflowOption.description}
                </Typography>
              </Grid>
              <Grid xs={12} item>
                <Switch
                  onBlur={handleBlur}
                  onChange={handleChange}
                  checked={values.default}
                  error={touched.default && errors.default}
                  label={
                    <>
                      <b>Default permission: </b>Users will have this permission
                      by default and it is not required to add users explicitly.
                      Revoking access can be done on a per-user basis.
                    </>
                  }
                  name='default'
                  color='primary'
                />
              </Grid>
              <Grid xs={12} item>
                <ImageUpload
                  ButtonProps={{ variant: 'contained', color: 'primary' }}
                  value={imageUploadState}
                  onChange={this.setImageUploadState}
                  required
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} autoFocus>
              Cancel
            </Button>
            <Button
              onClick={this.applyPermission}
              disabled={!isValid && !isCroppedImageUpdated}
              color='primary'
            >
              Apply
            </Button>
            <Button
              onClick={this.commitPermission}
              disabled={!isValid && !isCroppedImageUpdated}
              color='primary'
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

UpdatePermission.propTypes = {
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    scheduling: PropTypes.string.isRequired,
    workflow: PropTypes.string.isRequired,
    default: PropTypes.bool.isRequired
  }).isRequired,
  errors: PropTypes.shape({
    name: PropTypes.string,
    abbreviation: PropTypes.string,
    description: PropTypes.string,
    scheduling: PropTypes.string,
    workflow: PropTypes.string,
    default: PropTypes.bool
  }).isRequired,
  touched: PropTypes.shape({
    name: PropTypes.bool,
    abbreviation: PropTypes.bool,
    description: PropTypes.bool,
    scheduling: PropTypes.bool,
    workflow: PropTypes.bool,
    default: PropTypes.bool
  }).isRequired,
  bearerToken: PropTypes.string.isRequired,
  isUpdatingOne: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  setSubmitting: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  permission: PropTypes.shape({
    id: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    scheduling: PropTypes.string.isRequired,
    workflow: PropTypes.string.isRequired,
    default: PropTypes.bool.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.shape({
    dialogTitleLoading: PropTypes.string.isRequired,
    dialogContent: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ permission, auth }, { id }) => ({
  bearerToken: auth.token,
  isUpdatingOne: permission.isUpdatingOne,
  permission: permission.permissions[id]
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdatePermission: (id, entity) =>
    dispatch(updatePermission(id, entity))
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
  withFormik({
    displayName: 'UpdatePermission',
    handleSubmit: (values, { props, setSubmitting }) => {
      const { dispatchUpdatePermission, id } = props;
      dispatchUpdatePermission(id, values);
      setSubmitting(false);
    },
    mapPropsToValues: ({ permission }) => ({
      name: permission.name,
      abbreviation: permission.abbreviation,
      description: permission.description,
      scheduling: permission.scheduling,
      workflow: permission.workflow,
      default: permission.default,
      image: permission.image
    }),
    validationSchema: object().shape({
      name: string()
        .strict()
        .trim('The name must not start or end with spaces.')
        .required('The name is required.')
        .min(2, 'The name must be at least two characters long.'),
      abbreviation: string()
        .strict()
        .trim('The abbreviation must not be enclosed in whitespaces.')
        .required('The abbreviation is required.')
        .uppercase('The abbreviation must be uppercase.')
        .length(3, 'The abbreviation must be exactly three characters long.'),
      description: string()
        .strict()
        .trim('The abbreviation must not start or end with spaces.')
        .max(1000, 'The description must not be longer than 1000 characters.'),
      scheduling: string().required('Please select a scheduling option.'),
      workflow: string().required('Please select a workflow option.'),
      default: bool().required(
        'Please select if the permission should automatically be assigned to all users.'
      ),
      image: string().strict()
    })
  })
)(UpdatePermission);
