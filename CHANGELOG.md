# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.32.0] - 2020-10-31

### Added

- Improve menu UX on mobile and desktop
- Add indicators for current machine status

## [0.31.0] - 2020-10-31

### Added

- UI to select release channel
- Better UX to see if new sessions are available

## [0.30.3] - 2020-09-11

### Fixed

- Fixed feedback form and feedback page

## [0.30.2] - 2020-09-09

### Fixed

- Set machine status to `ready` if it is released

## [0.30.1] - 2020-09-01

### Added

- Display current user on Machine page
- Release button to unassign user from machine

## [0.30.0] - 2020-09-01

### Fixed

- Node page does not reload completely any more if a node is deleted
- Move delete button on Node page to prevent accidental deletion

### Added

- Display time of most recent session in nodes page
- Allow admins to manually override the node status

## [0.29.5] - 2020-08-31

### Added

- Add functionality to create machines
- Display machine names in node page

### Fixed

- User profile page

## [0.29.4] - 2020-08-26

### Added

- New `locking` scheduling for permissions

## [0.29.3] - 2020-08-26

### Removed

- Non-functional approvals page

## [0.29.2] - 2020-08-26

### Removed

- Non-functional booking page
- Non-functional report page

## [0.29.1] - 2020-03-18

### Fixed

- Fix application path by setting correct `homepage` in `package.json`

## [0.29.0] - 2020-03-18

### Changed

- Use `create-react-app` as base for `webpack` config
- Remove dummy content from dashboard
- Clean up menu for unauthenticated users

## [0.28.7] - 2019-11-15

### Fixed

- Updated dependencies

## [0.28.6] - 2019-09-26

### Added

- Admins can now edit user permissions via the user overview page

### Changed

- Bump Docker image node version to `10`

## [0.28.5] - 2019-07-28

### Fixed

- Issues in developer console about forward refs and wrong props

## [0.28.4] - 2019-06-25

### Fixed

- Implement authentication by passing webtoken via HTTP header

## [0.28.3] - 2019-05-22

### Added

- Display the user permission on profile page

## [0.28.2] - 2019-05-20

### Changed

- Adjust text in `<AboutBox />` to state the university and campus

## [0.28.1] - 2019-05-20

### Fixed

- Add `HUB_URL` to docker image to fix connection to websocket server

## [0.28.0] - 2019-05-20

### Added

- Livestreams for supported and configured machines

## [0.27.8] - 2019-05-19

### Fixed

- Fixed crash of tool page caused by having a user assigned to a tool

## [0.27.7] - 2019-05-13

### Changed

- Rebrand application to LabCloud

### Fixed

- Machine page is no longer blank if a machine has no assigned node

## [0.27.6] - 2019-05-05

### Added

- Overview for all tools of a tool type
- Possibility to create, read, update and delete tools

## [0.27.5] - 2019-05-02

### Fixed

- Accessing components unauthorized does not redirect to `/dashboard` anymore

## [0.27.4] - 2019-05-01

### Added

- Overview for different tool types, where tool types can be created, edited and deleted

### Fixed

- API calls to delete images are now properly authenticated

## [0.27.3] - 2019-04-26

### Changed

- Adjusted CI pipeline to `docker pull` before stopping and removing the old application to reduce downtime during deployment

## [0.27.2] - 2019-04-26

### Added

- Permissions can now reference images
- Images in permissions can be created, updated and deleted
- Useful component `<ImageUpload />`

## [0.27.1] - 2019-04-15

### Added

- Functional permission creation form
- Forms and buttons to delete and modify permissions

### Changed

- Improve `Select` component and migrate it to allow `variant="outlined"` style
- Adjusted display of permission

### Fixed

- Errors in developer console
- Loading of initial state from cookies

## [0.27.0] - 2019-01-22

### Added

- List machines from server in machine overview

## [0.26.0] - 2019-01-22

### Changed

- Bookings, Machines, Nodes, Profile and Users page have aligned usage of `ListItemText` and `TextField`

## [0.25.0] - 2019-01-21

### Added

- Add mocked incident report page

### Changed

- Menu profile button shows only first and last name without middle names

### Fixed

- Dashboard `BOOK` link leads to internal booking page
- Dashboard `HOW TO` link leads to external documentation page
- Expanding and reducing implemented for all expansion panels under `Bookings`, `Machines` and `Approvals`
- Fixed minor typo in `Help` page

## [0.24.0] - 2018-12-26

### Added

- Users with `admin` role can now update user roles via the user overview

### Fixed

- User overview displays correctly without throwing error

## [0.23.2] - 2018-12-18

### Added

- Improve mock values and align form with UI specification

## [0.23.1] - 2018-12-18

### Fixed

- Drill image is now visible

## [0.23.0] - 2018-12-18

### Added

- Dashboard page with mock data

## [0.22.0] - 2018-12-17

### Added

- Feedback form

## [0.21.0] - 2018-12-17

### Added

- Approvals page with mock data

## [0.20.0] - 2018-12-17

### Added

- Machines page with mock data

## [0.19.0] - 2018-12-17

### Added

- Permissions page with mock data

## [0.18.1] - 2018-12-14

### Fixed

- Remove unnecessary `.slice()` statement

## [0.18.0] - 2018-12-11

### Added

- Booking form and booking request overview with mock data

## [0.17.1] - 2018-12-11

### Fixed

- Adjust color of session help text in the case that no sessions are available

## [0.17.0] - 2018-12-11

### Added

- Node sessions can be authorized and identified

## [0.16.0] - 2018-12-06

### Added

- Node page to get an overview of all available nodes

### Fixed

- Login button on Dashboard has been enabled again
- Access to certain scenes is now controlled by the user role

## [0.15.0] - 2018-12-04

### Changed

- `AppLayout` components extracts `token` from URL hash
- Clicking the login button will instantly forward the user to the single-sign-on page

### Removed

- `Login` page to authorize application

## [0.14.2] - 2018-11-26

### Fixed

- Set up default value of `5000` for environment variable `POLL_INTERVAL` to prevent insanely frequent updates causing a laggy and unresponsive user interface

## [0.14.1] - 2018-11-24

### Changed

- Student card number only shows last digits on user profile and user detail overview

## [0.14.0] - 2018-11-22

### Added

- Update user profile in background (#14)

## [0.13.0] - 2018-11-20

### Added

- User overview page for admins (#10)

## [0.12.0] - 2018-11-19

### Added

- Informational box on dashboard to inform users about the purpose of the page (#7)

## [0.11.2] - 2018-11-09

### Fixed

- Approvals page is only visible for logged in users in sidebar menu
- Nodes page is only shown for users with role `staff`, `supervisor` or `admin` in sidebar menu

## [0.11.1] - 2018-11-09

### Fixed

- Removed dead code from previous Azure logout implementation
- Update dependencies

## [0.11.0] - 2018-11-06

### Added

- Machines page for future overview of physical machines

### Fixed

- Fixed layout breakpoints integrate smoothly with overall layout and do not jump any more

### Changed

- Use fixed layout on profile page to align overall UI feeling
- Renamed devices page to node page for future IoT node discovery and authorization

## [0.10.3] - 2018-11-05

### Fixed

- Post-deployment test delay has been increased to `20s` to ensure a stable reverse proxy setup before the test

## [0.10.2] - 2018-11-05

### Fixed

- Broken [CI configuration](./.gitlab-ci.yml) by missed out quotation mark (`"`)

## [0.10.1] - 2018-11-05

### Fixed

- Broken link to documentation by adding environment variable `DOCS_URL` in `.gitlab-ci.yml` (#5)

## [0.10.0] - 2018-11-05

### Added

- User profile shows card number

## [0.9.0] - 2018-11-05

### Changed

- Improve browser compatibility via `.browserslistrc`

### Added

- Feedback form on help page to submit issues to GitLab with simple issue preview
- Link to documentation on help page

## [0.8.0] - 2018-10-28

### Added

- Login page with login button
- Background logout via hidden `<iframe />`
- Profile view with basic user information
- Persistent login via usage of cookies
- HOC (higher order component) for protected views
- Automatic logout in case of unauthenticated requests

### Removed

- Unnecessary installation of `npm` CI to reduce deployment time

## [0.7.6] - 2018-10-14

### Fixed

- Removed scrollbar when layout should display content centered

## [0.7.5] - 2018-10-14

### Fixed

- Overflowing content in application layout displays proper scrollbar

## [0.7.4] - 2018-10-12

### Changed

- Silence `curl` output in CI pipeline

## [0.7.3] - 2018-10-12

### Fixed

- Typo in `CHANGELOG.md`

## [0.7.2] - 2018-10-12

### Changed

- Added delay before post-deployment test to allow deployment to settle

## [0.7.1] - 2018-10-12

### Fixed

- CI stage `deploy` requires `curl`, which was not previously installed

## [0.7.0] - 2018-10-12

### Changed

- Adjust page title to `SDU - The Core`
- Use different icon for devices

### Added

- Connect deployment to environment in GitLab

## [0.6.0] - 2018-10-12

### Added

- Set up `react-router-dom`
- Cross-linking of different views

## [0.5.1] - 2018-10-11

### Added

- Install dependencies via `npm ci` to enforce aligned `package-lock.json`

## [0.5.0] - 2018-10-11

### Added

- Deployment to Docker host via [.gitlab-ci.yml](./.gitlab-ci.yml)

### Changed

- Adjusted CI variable `PROD_API_URL` to be `API_URL` now

## [0.4.0] - 2018-10-08

### Added

- Expandable menu drawer
- Basic page layout

## [0.3.1] - 2018-10-08

### Fixed

- Removed warning about unsupported hot reloading from developer console

## [0.3.0] - 2018-10-08

### Added

- AppBar on front page
- `.env` file requires values `TAG` and `API_URL`

### Fixed

- `main` entry in `package.json` reflects entry for React application

### Changed

- Dependency upgrade check uses `david` instead of `npm-check-updates`
- Automatic tagging will check `version` in `package.json` and `package-lock.json`

## [0.2.0] - 2018-09-16

### Added

- Documentation for usage and tooling
- Cache for npm dependencies in CI
- Docker image build
- Automatic tagging in CI

## [0.1.0] - 2018-09-14

### Added

- [Webpack](https://webpack.js.org/) setup for common [React](https://reactjs.org/) app
- Setup of [Material UI](https://material-ui.com/)
- Linting with [ESLint](https://eslint.org/)
- Code formatting with [Prettier](https://prettier.io/)
- [MIT License](https://mit-license.org)
- CI with `.gitlab-ci.yml`
- Pre-commit hooks for formatting, linting and testing
- Hot module replacement via [react-hot-loader](https://github.com/gaearon/react-hot-loader)
