# LabCloud - Webapp

A web application to access the "LabCloud" manufacturing platform.

## Installation

Start out by creating a `.env` file in the project root with the following content:

```ini
# current application version
REACT_APP_TAG=dev
# REST API base url
REACT_APP_API_URL=http://localhost:9000
# documentation url
REACT_APP_DOCS_URL=http://localhost:8081
# polling interval for background updates
REACT_APP_POLL_INTERVAL=5000
```

Make sure to have at least the latest LTS of [nodejs](https://nodejs.org/en/) installed, then run:

```shell
$ npm i
```

Once you have the dependencies installed, you can run:

```shell
$ npm start
```

## Linting

In order to verify the code, you can run [ESLint](https://eslint.org) by typing:

```shell
$ npm run lint
```

## Formatting

If you are not using a plugin for your IDE to automatically format the code with [Prettier](https://prettier.io), you can run it via:

```shell
$ npm run format
```

## Testing

To run the unit test suite of this application, run:

```shell
$ npm run test:unit
```

## Build

To build the static content of this application in the `dist/` folder, run:

```shell
$ npm run build
```

## Debugging

For debugging purposes, it can be very useful to know the version and some environment specific parameters. This can be done by inspecting some global variables. Start out by opening your browsers developer console. This can often be done by hitting `F12`, while in your browser. Afterwards, navigate to a tab called console, type `TAG` and hit enter. This will return the current tag of the application that you are running. If it does not return anything, try it with `window.TAG`. The exposed variables are:

- `TAG`: current git tag of the application
- `API_URL`: remote REST API base URL

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org).
